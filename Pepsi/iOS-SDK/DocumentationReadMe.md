# Overview

The Footmarks iOS SDK includes everything you need to write apps that integrate with the Footmarks
SmartConnect™ beacon platform.

Included in the SDK are:

- The core framework, FTMCore.framework
- Two sample apps
- Documentation

Apps that integrate with the core framework can be written in Swift or Objective-C. An app's
deployment target must be iOS 9.0 or greater.


## Sample Apps

The easiest way to get started is by looking at the sample apps.

- **SampleAppSwift** - Demonstrates a basic integration with the core framework.
- **SampleAppObjC** - Equivalent to SampleAppSwift, but written in Objective-C.

In order to run a sample app, the core framework must be added as an embedded binary, which can be
done by following these steps:

1. Open one of the sample app projects in Xcode.
2. In the tree on the left, select the root node (e.g., SampleAppSwift).
3. In the content panel on the right, select the target.
4. Select the General tab.
5. Scroll to the "Embedded Binaries" section and click the + plus button.
6. Select "Add Other…"
7. Open FTMCore.framework, which is found in the root folder of the SDK.
8. When prompted, make sure "Copy items if needed" is checked, and select "Create groups."

The sample apps only work properly when run on a device because of the need for Bluetooth
functionality, which is not available in the simulator. Modify the project as needed to add a team
or to configure signing otherwise.

To run the sample apps, you also need to have a valid app key and secret. These values are passed
into the `FTMSession` class. Please contact Footmarks to request an app key and secret.


## Configuring Your Xcode Project

In order to integrate the core framework into your own app, these are the steps you must follow.

### 1. Embed FTMCore.framework

Just like the sample apps, your own app must embed the core framework. Follow the steps outlined
above in the Sample Apps section.

### 2. Configure Info.plist and background modes

The core framework requires the use of Bluetooth and location services. As a result, your app's
Info.plist file must include these four usage descriptions:

- **NSBluetoothPeripheralUsageDescription** - The core framework must be able to interact with
	Bluetooth peripherals in order to function properly.
- **NSLocationAlwaysUsageDescription** - The "always" mode of location services is required, as
	opposed to "when in use." Otherwise your app would not be notified when coming into range
	of beacons.
- **NSLocationAlwaysAndWhenInUseUsageDescription** - iOS 11 requires this key.
- **NSLocationWhenInUseUsageDescription** - iOS 11 requires this key.

Choose descriptions that are appropriate for your app.

If you intend to monitor beacons broadcasting in "payment" mode (rather than "private" mode), the
"bluetooth-central" background mode must be enabled. This can be done in one of two ways:

1. In Xcode, select the target, select Capabilities, turn on Background Modes, and check the box
	for "Uses Bluetooth LE accessories". Or…
2. In your Info.plist file, add UIBackgroundModes with an entry for "bluetooth-central".

### 3. Add a post build script

FTMCore.framework is a fat binary, which means it contains architectures for both simulator and
device. When submitting to the App Store, **Apple will not accept apps that have fat embedded
frameworks**. But it is convenient to have a single binary during development, rather than managing
separate frameworks for device and simulator.

To handle this, simply add a "run script" to your project. In Build Phases, click the + plus button
near the top of the screen and select "New Run Script Phase". Then copy in the following script.

````bash
#
# This script removes unused architectures from Footmarks frameworks, which are
# fat binaries that make it possible to develop apps in both the simulator and
# on device. Apple does not allow app store submissions to contain simulator
# architectures.
#
# In Xcode, select the target, then Build Phases, and make sure this script
# comes after the "Embed Frameworks" step.
#
# Based on:
# http://ikennd.ac/blog/2015/02/stripping-unwanted-architectures-from-dynamic-libraries-in-xcode/
#

APP_PATH="${TARGET_BUILD_DIR}/${WRAPPER_NAME}"
find "$APP_PATH" -name 'FTM*.framework' -type d | while read -r FRAMEWORK
do
	FRAMEWORK_EXECUTABLE_NAME=$(defaults read "$FRAMEWORK/Info.plist" CFBundleExecutable)
	FRAMEWORK_EXECUTABLE_PATH="$FRAMEWORK/$FRAMEWORK_EXECUTABLE_NAME"
	echo "Executable is $FRAMEWORK_EXECUTABLE_PATH"
	EXTRACTED_ARCHS=()

	for ARCH in $ARCHS
	do
		echo "Extracting $ARCH from $FRAMEWORK_EXECUTABLE_NAME"
		lipo -extract "$ARCH" "$FRAMEWORK_EXECUTABLE_PATH" -o "$FRAMEWORK_EXECUTABLE_PATH-$ARCH"
		EXTRACTED_ARCHS+=("$FRAMEWORK_EXECUTABLE_PATH-$ARCH")
	done

	echo "Merging extracted architectures: ${ARCHS}"
	lipo -o "$FRAMEWORK_EXECUTABLE_PATH-merged" -create "${EXTRACTED_ARCHS[@]}"
	rm "${EXTRACTED_ARCHS[@]}"

	echo "Replacing original executable with thinned version"
	rm "$FRAMEWORK_EXECUTABLE_PATH"
	mv "$FRAMEWORK_EXECUTABLE_PATH-merged" "$FRAMEWORK_EXECUTABLE_PATH"
done
````

### 4. Configure the app key and secret

In order to use the core framework, you must have an app key and secret. These values are passed
into the `FTMSession` class. Please contact Footmarks to request an app key and secret.



# Using the Core Framework

## Signing In

When integrating with the core framework, the first task that must be done is establishing a session
with the SmartConnect Cloud.

````swift
FTMSession.signIn(appKey: "…key…", appSecret: "…secret…", username: nil) {
	(error: Error?) in
}
````

The call to `signIn` can happen anywhere in the application flow, but it should be called before any
other core framework API. For background processing to work properly, such as when iOS launches the
app due to coming into range of a beacon, the `signIn` call needs to happen early on, such as in
`application(_:didFinishLaunchingWithOptions:)`

To obtain an app key and secret, please contact Footmarks.


## Receiving Experiences

Implement `FTMExperienceManagerDelegate` to receive experiences from the SmartConnect Cloud. For
details, see `FTMExperienceManagerDelegate.experienceManagerDidReceiveExperiences()`.


````swift
FTMExperienceManager.delegate = self

func experienceManagerDidReceiveExperiences(_ experiences: Set<FTMExperience>) {
	// Process the experiences.
}
````


## The Beacon Manager

Some apps might only be interested in processing experiences. Other apps might also want access to
the beacons in the surrounding environment. The `FTMBeaconManager` class maintains a list of
in-range beacons and communicates state changes with its delegate.

Any time the beacons in the surrounding environment change,
`FTMBeaconManagerDelegate.beaconManagerDidRangeBeacons()` is called. To access the latest beacons,
call `FTMBeaconManager.beacons`.


## Finding the Nearest Beacon

The `FTMNearestBeaconRequest` class allows a request to be made for the beacon that's nearest to the
current device. The beacons in the surrounding environment are analyzed, and the SmartConnect Cloud
determines which beacon is nearest.


## Migration Guide

Apps that have integrated with the Footmarks iOS SDK prior to version 6.0.0 will find that a number
of interfaces have changed. This is a high-level summary of the changes.

1. The framework is now an embedded binary, no longer statically linked.
2. The `FM` class prefix has been renamed to `FTM`.
3. Deprecated methods and properties have been removed.
4. All interfaces have been updated for Swift compatibility.
5. The beacon and experience classes have streamlined interfaces.
6. All network requests follow a common pattern for error handling and request canceling.
7. Sessions with the SmartConnect Cloud are now managed using the `FTMSession` class.
8. The delegates for beacon and experience management are now more flexible.

To migrate, start with updating to `FTMSession` for the initial sign-in. Then update to
`FTMBeaconManager` and `FTMExperienceManager` for managing beacons and experiences.


---

### Change Log
````
Version 6.0.0
- Initial release.

Version 6.1.0
- Add support for geofence regions.

Version 6.1.1
- Optimize location management.

Version 6.1.2
- Recompile with Xcode 9.1.

Version 6.1.3
- Minor updates to geofence region management and significant location changes.

Version 6.1.4
- Internal updates and improvements.

Version 6.2.0
- Enhancements to nearest beacon detection and beacon management.
````

//
//  LocationManager.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/3/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate, RegionsRequestDelegate {

	private var dateWhenErrorReported = Date(timeIntervalSinceReferenceDate: 0)
	private var didRangeCounter = 0
	private var error: Error?
	static let eventDidChangeAuthorizationStatus = Notification.Name("FTMCore.LocMgrDidChangeAuth")
	static let eventDidEnterCircularRegion = Notification.Name("FTMCore.LocMgrDidEntCircularRegion")
	static let eventDidExitBeaconRegion = Notification.Name("FTMCore.LocMgrDidExitBeaconRegion")
	static let eventDidExitCircularRegion = Notification.Name("FTMCore.LocMgrDidExitCircularRegion")
	static let eventDidFail = Notification.Name("FTMCore.LocMgrDidFail")
	static let eventDidUpdateBeacons = Notification.Name("FTMCore.LocMgrDidUpdateBeacons")
	static let eventDidUpdateSignificantLocation = Notification.Name("FTMCore.LocMgrDidUpdateSigLoc")
	private var location: (location: CLLocation, date: Date)?
	private var nativeBeaconsByRegionID = [String: Set<CLBeacon>]()
	private var nativeBeaconsHalfSecondAgo = [String: Set<CLBeacon>]()
	private let nativeManager = CLLocationManager()
	private var regionsRequest: RegionsRequest?
	private static let shared = LocationManager()
	private var significantLocation: (location: CLLocation, date: Date)?

	class var desiredAccuracy: CLLocationAccuracy {
		get {
			return shared.nativeManager.desiredAccuracy
		}
		set {
			shared.nativeManager.desiredAccuracy = newValue
		}
	}

	class var error: Error? {
		if !CLLocationManager.locationServicesEnabled() {
			return FTMError.locationServicesDisabled.error
		}
		if CLLocationManager.authorizationStatus() == .denied {
			return FTMError.locationServicesDenied.error
		}
		return shared.error
	}

	static var isPaused = true {
		willSet {
			guard isPaused != newValue else { return }

			if newValue {
				shared.nativeManager.stopUpdatingLocation()
				stopMonitoringAllFootmarksRegions()

				if CLLocationManager.significantLocationChangeMonitoringAvailable() {
					shared.nativeManager.stopMonitoringSignificantLocationChanges()
				}
			}
			else {
				shared.nativeManager.startUpdatingLocation()
				stopMonitoringAllFootmarksRegions()

				let regionsToMonitor =
					(Regions.beaconRegions as [CLRegion]) +
					(Regions.circularRegions as [CLRegion])

				for region in regionsToMonitor {
					shared.nativeManager.startMonitoring(for: region)
				}

				if CLLocationManager.significantLocationChangeMonitoringAvailable() {
					if Settings.monitorSignificantLocationChanges {
						if Constants.logLocationManager {
							NSLog("Starting to monitor significant location changes.")
						}
						shared.nativeManager.startMonitoringSignificantLocationChanges()
					}
				}
			}
		}
	}

	class var monitoredRegionIDs: [String] {
		var ids = [String]()
		for region in shared.nativeManager.monitoredRegions {
			if let beaconRegion = region as? CLBeaconRegion {
				ids.append(beaconRegion.proximityUUID.uuidString)
			}
			else {
				ids.append(region.identifier)
			}
		}
		return ids
	}

	class var nativeBeacons: [(beacon: CLBeacon, regionIDs: Set<String>)] {
		return nativeBeaconsTransformed(input: shared.nativeBeaconsByRegionID)
	}

	class var nativeBeaconsHalfSecondAgo: [(beacon: CLBeacon, regionIDs: Set<String>)] {
		return nativeBeaconsTransformed(input: shared.nativeBeaconsHalfSecondAgo)
	}

	class var recentLocation: (location: CLLocation, date: Date)? {
		return shared.location
	}

	private override init() {
		super.init()

		if CLLocationManager.authorizationStatus() == .notDetermined {
			nativeManager.requestAlwaysAuthorization()
		}

		nativeManager.delegate = self
		nativeManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers

		let nc = NotificationCenter.default
		nc.addObserver(self, selector: #selector(onRegionsDidUpdate),
			name: Regions.eventDidUpdate, object: nil)
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	private class func isFootmarksRegionID(_ id: String) -> Bool {

		// Prior to December 2017, Footmarks beacon regions used a "Footmarks" prefix, and
		// Footmarks circular regions had a generig UUID syntax. Moving forward, all
		// Footmarks regions, including circular regions, use the prefix found in the
		// Constants class.

		return id.hasPrefix(Constants.regionIDPrefix) || id.hasPrefix("Footmarks")
	}

	func locationManager(_ manager: CLLocationManager, didChangeAuthorization
		status: CLAuthorizationStatus)
	{
		assert(Thread.isMainThread)
		let nc = NotificationCenter.default
		nc.post(name: LocationManager.eventDidChangeAuthorizationStatus, object: self)
	}

	func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState,
		for region: CLRegion)
	{
		assert(Thread.isMainThread)
		switch state {
			case .inside:
				let beaconRegion = region as? CLBeaconRegion
				if Constants.logLocationManager {
					let suffix = (beaconRegion == nil) ? "" : " Starting to range."
					NSLog("Region \(region.identifier) state = inside." + suffix)
				}
				if let beaconRegion = beaconRegion {
					nativeManager.startRangingBeacons(in: beaconRegion)
				}
			case .outside:
				if Constants.logLocationManager {
					NSLog("Region \(region.identifier) state = outside.")
				}
			case .unknown:
				if Constants.logLocationManager {
					NSLog("Region \(region.identifier) state = unknown.")
				}
		}
	}

	func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
		assert(Thread.isMainThread)
		if let beaconRegion = region as? CLBeaconRegion {
			if Constants.logLocationManager {
				NSLog("Entering region \(beaconRegion.identifier).")
			}
			nativeManager.startRangingBeacons(in: beaconRegion)
		}
		else if let circularRegion = region as? CLCircularRegion {
			if Constants.logLocationManager {
				NSLog("Entering circular region \(circularRegion.identifier).")
			}
			if Regions.isKnown(circularRegion: circularRegion) {
				NotificationCenter.default.post(name: LocationManager.eventDidEnterCircularRegion,
					object: self, userInfo: ["circularRegion": circularRegion])
			}
		}
	}

	func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
		assert(Thread.isMainThread)
		if let beaconRegion = region as? CLBeaconRegion {
			if Constants.logLocationManager {
				NSLog("Exiting region \(beaconRegion.identifier).")
			}
			nativeManager.stopRangingBeacons(in: beaconRegion)
			NotificationCenter.default.post(name: LocationManager.eventDidExitBeaconRegion,
				object: self, userInfo: ["beaconRegion": beaconRegion])
		}
		else if let circularRegion = region as? CLCircularRegion {
			if Constants.logLocationManager {
				NSLog("Exiting circular region \(circularRegion.identifier).")
			}
			if Regions.isKnown(circularRegion: circularRegion) {
				NotificationCenter.default.post(name: LocationManager.eventDidExitCircularRegion,
					object: self, userInfo: ["circularRegion": circularRegion])
			}
		}
	}

	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		assert(Thread.isMainThread)

		if let error = error as? CLError, error.code == .locationUnknown {

			// This isn't really an error. Apple's documentation states, "If the location service
			// is unable to retrieve a location right away, it reports a kCLErrorLocationUnknown
			// error and keeps trying. In such a situation, you can simply ignore the error and
			// wait for a new event.

		}
		else {
			self.error = error
			if Constants.logLocationManager {
				NSLog("Location manager error: \(error)")
			}

			let now = Date()
			let delta = now.timeIntervalSince(dateWhenErrorReported)

			guard delta > 0.5 else {

				// When the user denies location services, a situation can arise where many errors
				// happen in a short amount of time (one for each failed monitoring of a region).
				// Only communicate the first error to the delegate.

				return
			}

			dateWhenErrorReported = now
			NotificationCenter.default.post(name: LocationManager.eventDidFail, object: self)
		}
	}

	func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon],
		in region: CLBeaconRegion)
	{
		assert(Thread.isMainThread)
		var uniqueBeacons = Set<CLBeacon>()

		for beacon in beacons {
			if Constants.logLocationManager {
				NSLog("Ranging \(region.identifier). Beacon = (\(beacon.major),\(beacon.minor))")
			}

			var found = false
			for ub in uniqueBeacons where ub.major == beacon.major && ub.minor == beacon.minor {
				// For some reason this happens from time to time. Don't add the duplicate.
				found = true
				break
			}

			if !found {
				uniqueBeacons.insert(beacon)
			}
		}

		nativeBeaconsByRegionID[region.identifier] = uniqueBeacons
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			self.nativeBeaconsHalfSecondAgo[region.identifier] = uniqueBeacons
		}

		// Notify that the beacons have been updated, but only if another update doesn't happen in
		// the very near future. In practice what happens is that this method (didRangeBeacons)
		// gets called back-to-back, once per region, which is a little too chatty for the purposes
		// of our delegate, which doesn't care about regions.

		didRangeCounter += 1
		let frozenDidRangeCounter = didRangeCounter
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
			if self.didRangeCounter == frozenDidRangeCounter {
				let nc = NotificationCenter.default
				nc.post(name: LocationManager.eventDidUpdateBeacons, object: self)
			}
		}
	}

	func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
		if Constants.logLocationManager {
			NSLog("Starting to monitor \(region.identifier).")
		}
		manager.requestState(for: region)
	}

	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		assert(Thread.isMainThread)

		// Apple's documentation states, "This array always contains at least one object ... the
		// most recent location update is at the end of the array."

		guard let location = locations.last else {
			assertionFailure("The location is nil!")
			return
		}

		self.location = (location, Date())
		error = nil

		if significantLocation == nil {
			significantLocation = self.location
		}

		if Constants.logLocationManager {
			NSLog("Current location = \(location)")
		}

		if !RegionsRequest.didMakeRequest && regionsRequest == nil {
			regionsRequest = RegionsRequest(delegate: self)
		}

		if let sigLoc = significantLocation, Settings.monitorSignificantLocationChanges {
			let deltaDist = sigLoc.location.distance(from: location)
			let deltaTime = abs(sigLoc.date.timeIntervalSinceNow)

			if deltaDist >= 200 && deltaTime >= 60 {

				// The distance and time have changed enough since the last event. Go ahead and
				// treat this as a signification location change.

				if Constants.logLocationManager {
					NSLog("The location has significantly changed.")
				}

				significantLocation = (location, Date())
				if regionsRequest == nil {
					regionsRequest = RegionsRequest(delegate: self)
				}
				let nc = NotificationCenter.default
				nc.post(name: LocationManager.eventDidUpdateSignificantLocation, object: self)
			}
		}
	}

	func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor
		region: CLBeaconRegion, withError error: Error)
	{
		assert(Thread.isMainThread)
		self.error = error
		if Constants.logLocationManager {
			NSLog("Location manager ranging error: \(error)")
		}
		NotificationCenter.default.post(name: LocationManager.eventDidFail, object: self)
	}

	private class func nativeBeaconsTransformed(input: [String: Set<CLBeacon>])
		-> [(beacon: CLBeacon, regionIDs: Set<String>)]
	{
		var dict = [Int: (beacon: CLBeacon, regionIDs: Set<String>)]()
		for (regionID, beacons) in input {
			for beacon in beacons {
				let key = beacon.major.intValue << 16 + beacon.minor.intValue
				if var item = dict[key] {
					item.regionIDs.insert(regionID)
					dict[key] = item
				}
				else {
					dict[key] = (beacon, [regionID])
				}
			}
		}
		return Array(dict.values)
	}

	@objc
	private func onRegionsDidUpdate() {
		guard !LocationManager.isPaused else { return }

		let regionsToMonitor =
			(Regions.beaconRegions as [CLRegion]) +
			(Regions.circularRegions as [CLRegion]) +
			(Regions.hyperRegions as [CLRegion])

		// iOS allows up to 20 regions to be monitored.
		assert(regionsToMonitor.count <= 20)

		// Stop monitoring Footmarks regions that we no longer care about.

		for monitoredRegion in nativeManager.monitoredRegions {
			var found = false
			for region in regionsToMonitor {
				if monitoredRegion.identifier == region.identifier {
					found = true
					break
				}
			}
			if !found && LocationManager.isFootmarksRegionID(monitoredRegion.identifier) {
				if Constants.logLocationManager {
					NSLog("Stopping the monitoring of \(monitoredRegion.identifier).")
				}
				nativeManager.stopMonitoring(for: monitoredRegion)
			}
		}

		// Register the regions that we do care about.

		for region in regionsToMonitor {
			var found = false
			for monitoredRegion in nativeManager.monitoredRegions {
				if monitoredRegion.identifier == region.identifier {
					found = true
					break
				}
			}
			if !found {
				if Constants.logLocationManager {
					NSLog("Starting the monitoring of \(region.identifier).")
				}
				nativeManager.startMonitoring(for: region)
			}
		}
	}

	func regionsRequestDidFinish(_ request: RegionsRequest, error: Error?) {
		if let error = error, Constants.logLocationManager {
			NSLog("Regions request error: \(error)")
		}
		regionsRequest = nil
	}

	class func removeNativeBeacon(major: Int, minor: Int) {
		for (regionID, beacons) in shared.nativeBeaconsByRegionID {
			let beacons = beacons.filter { $0.major.intValue != major || $0.minor.intValue != minor }
			shared.nativeBeaconsByRegionID[regionID] = Set(beacons)
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
				shared.nativeBeaconsHalfSecondAgo[regionID] = Set(beacons)
			}
		}
	}

	private class func stopMonitoringAllFootmarksRegions() {
		for region in shared.nativeManager.monitoredRegions {
			if isFootmarksRegionID(region.identifier) {
				shared.nativeManager.stopMonitoring(for: region)
			}
		}
	}

}

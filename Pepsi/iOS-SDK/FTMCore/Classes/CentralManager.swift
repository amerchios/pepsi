//
//  CentralManager.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/4/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreBluetooth
import CoreLocation

class CentralManager: NSObject, CBCentralManagerDelegate {

	static let eventDidUpdatePeripherals = Notification.Name("FTMCore.CMDidUpdatePeripherals")
	static let eventDidUpdateState = Notification.Name("FTMCore.CMDidUpdateState")
	private static let lock = NSObject()
	private let nativeManager: CBCentralManager
	private var peripheralsByServiceID = [String: Peripheral]()
	private let queue = DispatchQueue(label: "FTMCore.CentralManager")
	private var serviceIDs = [String: Date]()
	private static let shared = CentralManager()

	static var isPaused = true {
		willSet {
			guard isPaused != newValue else { return }
			shared.queue.async {
				if newValue {
					shared.nativeManager.stopScan()
				}
				else if state == .poweredOn {
					shared.scan()
				}
			}
		}
	}

	class var peripheralsByServiceID: [String: Peripheral] {
		objc_sync_enter(lock)
		let dict = shared.peripheralsByServiceID
		objc_sync_exit(lock)
		return dict
	}

	class var state: CBCentralManagerState {
		switch shared.nativeManager.state {
			case .poweredOff:
				return .poweredOff
			case .poweredOn:
				return .poweredOn
			case .resetting:
				return .resetting
			case .unauthorized:
				return .unauthorized
			case .unknown:
				return .unknown
			case .unsupported:
				return .unsupported
		}
	}

	private override init() {
		nativeManager = CBCentralManager(delegate: nil, queue: queue, options: [
			CBCentralManagerOptionShowPowerAlertKey: false
		])
		super.init()
		nativeManager.delegate = self
	}

	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
		advertisementData: [String: Any], rssi RSSI: NSNumber)
	{
		if Constants.logCentralManager {
			if advertisementData[CBAdvertisementDataManufacturerDataKey] != nil {
				NSLog("\n    Name: '%@'\n    ID: %@\n    RSSI: %@\n    Advertisement Data: %@",
					peripheral.name ?? "",
					peripheral.identifier.uuidString,
					RSSI,
					advertisementData)
			}
		}

		guard
			let header = advertisementData[CBAdvertisementDataManufacturerDataKey] as? Data,
			let serviceIDs = advertisementData[CBAdvertisementDataServiceUUIDsKey] as? [CBUUID],
			header.count == 8,
			!serviceIDs.isEmpty
		else {
			return
		}

		let hexHeader = AGKHex.string(data: header)
		let index = hexHeader.index(hexHeader.startIndex, offsetBy: 4)
		let macAddress = hexHeader[index...].lowercased()
		let rssi = RSSI.doubleValue
		let serviceID = serviceIDs[0].uuidString
		let nc = NotificationCenter.default

		objc_sync_enter(CentralManager.lock)
		let peripheral = peripheralsByServiceID[serviceID]
		objc_sync_exit(CentralManager.lock)

		if let peripheral = peripheral {
			peripheral.updateValues(byte0: header[0], byte1: header[1],
				rssi: rssi, serviceID: serviceID)
			nc.post(name: CentralManager.eventDidUpdatePeripherals, object: self)
		}
		else if let peripheral = Peripheral(byte0: header[0], byte1: header[1],
			macAddress: macAddress, rssi: rssi, serviceID: serviceID)
		{
			objc_sync_enter(CentralManager.lock)

			// Remove expired peripherals.
			for p in peripheralsByServiceID.values where p.isExpired {
				peripheralsByServiceID[p.serviceID] = nil
			}

			// Add the new peripheral.
			peripheralsByServiceID[serviceID] = peripheral

			objc_sync_exit(CentralManager.lock)
			nc.post(name: CentralManager.eventDidUpdatePeripherals, object: self)
		}
	}

	func centralManagerDidUpdateState(_ central: CBCentralManager) {
		if Constants.logCentralManager {
			NSLog("The central manager's new state is \(central.state.rawValue)")
		}

		if central.state == .poweredOn && !CentralManager.isPaused {
			scan()
		}

		NotificationCenter.default.post(name: CentralManager.eventDidUpdateState, object: self)
	}

	private func scan() {

		//
		// The docs say:
		//
		//     "Apps that have specified the bluetooth-central background mode are allowed to
		//     scan while in the background. That said, they must explicitly scan for one or more
		//     services by specifying them in the serviceUUIDs parameter."
		//
		// Since Footmarks beacons rotate the service ID, we need to keep track of a list of
		// service IDs to scan for. Service IDs are derived from CLBeacon objects.
		//

		let ids = serviceIDs.map { CBUUID(string: $0.key) }
		if Constants.logCentralManager {
			NSLog("Scanning for service IDs: \(ids)")
		}
		nativeManager.scanForPeripherals(withServices: ids, options: [
			CBCentralManagerScanOptionAllowDuplicatesKey: true
		])
	}

	class func updateNativeBeacons(_ nativeBeacons: Set<CLBeacon>) {

		// Add the service IDs associated with the given native beacons to the master list. If a
		// service ID hasn't been heard from in awhile, stop scanning for that service.

		let serviceIDsPrev = Set(shared.serviceIDs.keys)

		for (serviceID, dateOfLastImpression) in shared.serviceIDs {
			if dateOfLastImpression.timeIntervalSinceNow < -60.0 {
				if Constants.logCentralManager {
					NSLog("No longer scanning for service ID \(serviceID)")
				}
				shared.serviceIDs[serviceID] = nil
			}
		}

		for nativeBeacon in nativeBeacons {
			let serviceID = FTMBeacon.peripheralServiceID(nativeBeacon: nativeBeacon)
			shared.serviceIDs[serviceID] = Date()
		}

		let serviceIDsCurr = Set(shared.serviceIDs.keys)
		guard serviceIDsCurr != serviceIDsPrev else { return }

		if Constants.logCentralManager {
			NSLog("The service IDs have changed.")
		}

		if shared.nativeManager.state == .poweredOn && !CentralManager.isPaused {
			shared.scan()
		}
	}

}

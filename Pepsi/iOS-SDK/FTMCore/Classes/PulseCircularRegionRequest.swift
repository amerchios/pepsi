//
//  PulseCircularRegionRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 10/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation

protocol PulseCircularRegionRequestDelegate: class {
	func pulseCircularRegionRequestDidFinish(_ request: PulseCircularRegionRequest, error: Error?)
}

class PulseCircularRegionRequest: WebServiceRequest {

	enum RegionState: String {
		case enter = "enter"
		case exit = "exit"
	}

	private weak var delegate: PulseCircularRegionRequestDelegate?
	private(set) var experiences = Set<FTMExperience>()
	private weak var originalRequest: PulseCircularRegionRequest?
	private let region: CLCircularRegion
	private let regionState: RegionState
	private var request2: PulseCircularRegionRequest?

	init(delegate: PulseCircularRegionRequestDelegate, region: CLCircularRegion,
		regionState: RegionState)
	{
		self.delegate = delegate
		self.region = region
		self.regionState = regionState
		var body: [String: Any] = [
			"geofence": region.identifier,
			"regionState": regionState.rawValue,
			"type": "geofence",
		]
		body = WebServiceRequest.addLocation(body: body)
		super.init(path: "pulse", body: body)
	}

	private func makeSecondRequest() {
		guard let delegate = delegate else { return }
		let request = PulseCircularRegionRequest(delegate: delegate,
			region: region, regionState: regionState)
		request2 = request
		request.originalRequest = self
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		weak var wself = self

		if let error = error,
			(error as NSError).domain == FTMError.domain,
			(error as NSError).code == FTMError.authFailed.rawValue,
			let credentials = FTMSession.recentCredentials,
			originalRequest == nil
		{
			// The request failed due to an auth failure. This should be rare, but if it
			// happens, sign in using the existing in-memory credentials, then try the pulse
			// request one more time in case the sign in is able to refresh the auth token.

			FTMSession.signIn(
				appKey: credentials.appKey,
				appSecret: credentials.appSecret,
				username: credentials.username,
				completion: {
					(error: Error?) in
					wself?.makeSecondRequest()
				}
			)

			return
		}

		experiences = PulseBeaconRequest.parseExperiencesFromServerDictionary(dict)
		originalRequest?.experiences = experiences
		delegate?.pulseCircularRegionRequestDidFinish(originalRequest ?? self, error: error)
	}

}

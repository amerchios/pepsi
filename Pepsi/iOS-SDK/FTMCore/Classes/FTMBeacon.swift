//
//  FTMBeacon.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/4/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation

/// A Footmarks beacon.
public class FTMBeacon: NSObject {

	enum RegionState {

		case enter, exit, idle

		var string: String {
			switch self {
				case .enter:
					return "enter"
				case .exit:
					return "exit"
				case .idle:
					return "idle"
			}
		}

	}

	private var cachedValues = (accuracy: CLLocationAccuracy(-1), rssi: 0)
	private var dateOfCachedValues = Date()
	private var dateOfLastImpression = Date()
	var dateOfNextPulse = Date()
	private(set) var isExpiredByForce = false
	private(set) var isUnauthorized: Bool
	private var nativeBeacon: CLBeacon
	private var peripheral: Peripheral?
	private(set) var regionIDs: Set<String>
	var regionState = RegionState.enter
	private var rssiConstant: Double?
	private static var serviceIDCache = [String: String]()

	/// The accuracy of the underlying `CLBeacon`.
	@objc
	public var accuracy: CLLocationAccuracy {
		updateCachedValues()
		return nativeBeacon.accuracy
	}

	var accuracyCached: CLLocationAccuracy {
		updateCachedValues()
		return cachedValues.accuracy
	}

	var accuracyCachedUsingFootmarksAlgorithm: CLLocationAccuracy {
		guard let rssiConstant = rssiConstant, rssiConstant < 0 else {
			// Sometimes iOS reports an RSSI of zero.
			return accuracyCached
		}

		let rssi = Double(rssiCached)

		if rssi == 0 {
			// The RSSI value is not available.
			return accuracyCached
		}

		let ratio = rssi / rssiConstant

		if ratio < 1 {
			return pow(ratio, 10)
		}

		return 0.89976 * pow(ratio, 7.7095) + 0.111
	}

	/**
		The attributes configured on the management console.

		Attributes are a flexible way to specify metadata for a beacon. They can be used as
		filters when finding the nearest beacon.

		- seealso:
			`FTMNearestBeaconRequest.begin(attributeFilterKey:attributeFilterValue:completion:)`
	*/
	@objc
	public private(set) var attributes: [String: String]

	var batteryLevel: Double? {
		return peripheral?.batteryLevel
	}

	/// The hash value.
	@objc
	public override var hashValue: Int {
		return id.hashValue ^ major.hashValue ^ minor.hashValue
	}

	/// The unique identifier for the beacon.
	@objc
	public private(set) var id: String

	var isExpiredNaturally: Bool {
		return FTMBeacon.isExpired(dateOfLastImpression: dateOfLastImpression)
	}

	/// The MAC address.
	@objc
	public let macAddress: String

	var major: Int {
		return nativeBeacon.major.intValue
	}

	var minor: Int {
		return nativeBeacon.minor.intValue
	}

	/// The name of the beacon.
	///
	/// Generally the name will be the same as the beacon's `macAddress`, although beacon names
	/// can be customized on the management console.
	@objc
	public private(set) var name: String

	var peripheralServiceID: String {
		return FTMBeacon.peripheralServiceID(nativeBeacon: nativeBeacon)
	}

	/// The proximity of the underlying `CLBeacon`.
	@objc
	public var proximity: CLProximity {
		return nativeBeacon.proximity
	}

	var proximityUUID: UUID {
		return nativeBeacon.proximityUUID
	}

	/// The RSSI value of the underlying `CLBeacon`.
	@objc
	public var rssi: Int {
		updateCachedValues()
		return nativeBeacon.rssi
	}

	var rssiCached: Int {
		updateCachedValues()
		return cachedValues.rssi
	}

	init(nativeBeacon: CLBeacon,
		attributes: [String: String],
		id: String,
		isUnauthorized: Bool,
		macAddress: String,
		name: String,
		regionIDs: Set<String>,
		rssiConstant: Double)
	{
		self.attributes = attributes
		self.id = id
		self.isUnauthorized = isUnauthorized
		self.macAddress = macAddress
		self.name = name
		self.nativeBeacon = nativeBeacon
		self.peripheral = nil
		self.regionIDs = regionIDs
		self.rssiConstant = rssiConstant
	}

	init(nativeBeacon: CLBeacon, peripheral: Peripheral, regionIDs: Set<String>) {
		self.attributes = [:]
		self.id = ""
		self.isUnauthorized = true
		self.macAddress = peripheral.macAddress
		self.name = ""
		self.nativeBeacon = nativeBeacon
		self.peripheral = peripheral
		self.regionIDs = regionIDs
	}

	/// Evaluates two beacons for equality.
	public static func ==(lhs: FTMBeacon, rhs: FTMBeacon) -> Bool {
		return
			lhs.attributes == rhs.attributes &&
			lhs.dateOfLastImpression == rhs.dateOfLastImpression &&
			lhs.dateOfNextPulse == rhs.dateOfNextPulse &&
			lhs.id == rhs.id &&
			lhs.name == rhs.name &&
			lhs.nativeBeacon == rhs.nativeBeacon &&
			lhs.peripheral == rhs.peripheral &&
			lhs.regionState == rhs.regionState
	}

	func expireByForce() {
		isExpiredByForce = true
	}

	class func isExpired(dateOfLastImpression: Date) -> Bool {
		return dateOfLastImpression.timeIntervalSinceNow <= -30.0
	}

	static func openID(nativeBeacon: CLBeacon) -> String {
		let b = nativeBeacon
		let proxID = b.proximityUUID.uuidString.replacingOccurrences(of: "-", with: "")
		let s = "\(b.major.intValue):\(b.minor.intValue):\(proxID)".uppercased()
		let data = s.data(using: .utf8)!
		let hashed = __FTMPrivate.__a(100, __b: data) as! Data
		return AGKHex.string(data: hashed).lowercased()
	}

	class func peripheralServiceID(nativeBeacon: CLBeacon) -> String {
		let major = nativeBeacon.major.intValue
		let minor = nativeBeacon.minor.intValue
		let key = "\(major):\(minor)"

		if let serviceID = serviceIDCache[key] {
			// Calculating the service ID is a bit expensive, so maintain a cache.
			return serviceID
		}

		let data = __FTMPrivate.__a(200, __b: key) as! Data
		assert(data.count == 16)

		// Reverse the bytes.

		var dataReversed = Data()
		for b in data.reversed() {
			dataReversed.append(contentsOf: [b])
		}

		// Convert to hex and insert hyphens like a UUID.

		let s = AGKHex.string(data: dataReversed)
		var serviceID = ""
		for (i, c) in s.enumerated() {
			serviceID.append(c)
			if i == 7 || i == 11 || i == 15 || i == 19 {
				serviceID.append("-")
			}
		}

		while serviceIDCache.count > 256 {
			// Don't let the cache grow too big.
			serviceIDCache.remove(at: serviceIDCache.startIndex)
		}

		serviceIDCache[key] = serviceID
		return serviceID
	}

	func update(attributes: [String: String], id: String, isUnauthorized: Bool,
		name: String, rssiConstant: Double)
	{
		self.attributes = attributes
		self.id = id
		self.isUnauthorized = isUnauthorized
		self.name = name
		self.rssiConstant = rssiConstant
	}

	func update(nativeBeacon: CLBeacon, peripheral: Peripheral?, regionIDs: Set<String>) {
		self.nativeBeacon = nativeBeacon
		self.peripheral = peripheral
		self.regionIDs = regionIDs
		dateOfLastImpression = Date()
	}

	private func updateCachedValues() {
		if nativeBeacon.accuracy >= 0 {
			// The accuracy is valid. Update the cached values with the latest good data.
			cachedValues.accuracy = nativeBeacon.accuracy
			cachedValues.rssi = nativeBeacon.rssi
			dateOfCachedValues = Date()
		}
		else if Date().timeIntervalSince(dateOfCachedValues) > 5 {
			// Too much time has passed for the cached values to be meaningful.
			cachedValues.accuracy = nativeBeacon.accuracy
			cachedValues.rssi = nativeBeacon.rssi
		}
		else {
			// Keep the cached values around for a few seconds.
		}
	}

}

//
//  FTMNearestBeaconResult.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/16/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/**
	The result of a request to find the nearest beacon.
	- seealso:
		- `FTMNearestBeaconRequest`
		- `FTMNearestBeaconAcknowledgment`
*/
public class FTMNearestBeaconResult: NSObject {

	/// The nearest beacon.
	@objc
	public let beacon: FTMBeacon

	/// A value between `0.0` and `1.0` representing the confidence level that the given beacon is
	/// truly the nearest.
	@objc
	public let confidence: Double

	/// The result ID, used for sending an acknowledgment with `FTMNearestBeaconAcknowledgment`.
	@objc
	public let id: String

	private(set) static var recentResults = [FTMNearestBeaconResult]()

	init(beacon: FTMBeacon, confidence: Double, id: String) {
		self.beacon = beacon
		self.confidence = confidence
		self.id = id

		super.init()

		// Keep track of recent results, which FTMNearestBeaconAcknowledgment uses.

		FTMNearestBeaconResult.recentResults.insert(self, at: 0)
		while FTMNearestBeaconResult.recentResults.count > 100 {
			FTMNearestBeaconResult.recentResults.removeLast()
		}
	}

}

//
//  DemoLogger.swift
//  FTMCore
//
//  Created by Shane Meyer on 7/24/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

class DemoLogger {

	private static var messages = [(String, Date)]()
	fileprivate static var messagesForServer = [(String, Date)]()
	fileprivate static var request: DemoLogRequest?
	private static let shared = DemoLogger()

	private static let dateFormatter: DateFormatter = {
		let fmt = DateFormatter()
		fmt.dateFormat = "HH:mm:ss.SSS"
		return fmt
	}()

	private init() {
		let nc = NotificationCenter.default
		nc.addObserver(self, selector: #selector(onGetLogMessagesAsString0),
			name: Notification.Name("__FTMPrivateGetLogMessagesAsString0"), object: nil)
		nc.addObserver(self, selector: #selector(onLog(_:)),
			name: Notification.Name("__FTMPrivateLogMessage"), object: nil)
		DemoLogger.doRequestIfNeeded()
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	fileprivate class func doRequestIfNeeded() {
		DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
			if messagesForServer.isEmpty {
				DemoLogger.doRequestIfNeeded()
			}
			else {
				request = DemoLogRequest()
			}
		}
	}

	class var messagesAsString: String {
		let array = messages.map { dateFormatter.string(from: $0.1) + " " + $0.0 }
		return array.joined(separator: "\n")
	}

	fileprivate class var messagesForServerAsString: String {
		let array = messagesForServer.map { dateFormatter.string(from: $0.1) + " " + $0.0 }
		return array.joined(separator: "\n")
	}

	class func go() {
		_ = shared
	}

	class func log(_ message: String) {
		let item = (message, Date())
		messages.append(item)
		messagesForServer.append(item)
	}

	@objc
	private func onGetLogMessagesAsString0() {
		let nc = NotificationCenter.default
		nc.post(name: Notification.Name("__FTMPrivateGetLogMessagesAsString1"), object: nil,
			userInfo: ["messages": DemoLogger.messagesAsString])
	}

	@objc
	private func onLog(_ notification: Notification) {
		if let message = notification.userInfo?["message"] as? String {
			DemoLogger.log(message)
		}
	}

}

private class DemoLogRequest: WebServiceRequest {

	init() {
		super.init(demoLogMessages: DemoLogger.messagesForServerAsString)
		DemoLogger.messagesForServer = []
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		if let error = error {
			NSLog("Logging error: \(error)")
		}
		DemoLogger.doRequestIfNeeded()
	}

}

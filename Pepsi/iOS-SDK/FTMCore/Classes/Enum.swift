//
//  Enum.swift
//  FTMCore
//
//  Created by Shane Meyer on 6/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

enum RegionType: Int {

	case `private`, secure

	init?(serverValue: String) {
		switch serverValue {
			case "private":
				self = .private
			case "secure":
				self = .secure
			default:
				return nil
		}
	}

}

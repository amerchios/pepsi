//
//  ValidateRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/12/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation

protocol ValidateRequestDelegate: class {
	func validateRequestDidFinish(_ request: ValidateRequest, error: Error?)
}

class ValidateRequest: WebServiceRequest {

	let beacon: FTMBeacon?
	private weak var delegate: ValidateRequestDelegate?
	let nativeBeacon: CLBeacon?
	let openID: String?
	private weak var originalRequest: ValidateRequest?
	let regionIDs: Set<String>
	private var request2: ValidateRequest?

	private(set) var response: (
		attributes: [String: String],
		id: String,
		isUnauthorized: Bool,
		macAddress: String,
		name: String,
		registerHyperRegion: Bool,
		rssiConstant: Double
	)?

	init(delegate: ValidateRequestDelegate, beacon: FTMBeacon, regionIDs: Set<String>) {
		self.beacon = beacon
		self.delegate = delegate
		self.nativeBeacon = nil
		self.openID = nil
		self.regionIDs = regionIDs

		let macAddress = beacon.macAddress
		let body = ValidateRequest.addCommonBodyItems(body: ["macs": [macAddress]], beacon: beacon)
		super.init(path: "validate/beacons", body: body)
		expectedResponseFormat = .array
	}

	init(delegate: ValidateRequestDelegate, nativeBeacon: CLBeacon, openID: String,
		regionIDs: Set<String>)
	{
		self.beacon = nil
		self.delegate = delegate
		self.nativeBeacon = nativeBeacon
		self.openID = openID
		self.regionIDs = regionIDs

		let body = ValidateRequest.addCommonBodyItems(body: ["opens": [openID]], beacon: nil)
		super.init(path: "validate/beacons", body: body)
		expectedResponseFormat = .array
	}

	private static func addCommonBodyItems(body: [String: Any], beacon: FTMBeacon?)
		-> [String: Any]
	{
		var body = body
		if let batteryLevel = beacon?.batteryLevel {
			body["battery"] = Int(round(batteryLevel * 100.0))
		}
		body["unauthorizedSupport"] = true
		body = WebServiceRequest.addLocation(body: body)
		return body
	}

	private func makeSecondRequest() {
		guard let delegate = delegate else { return }

		if let beacon = beacon {
			request2 = ValidateRequest(delegate: delegate, beacon: beacon, regionIDs: regionIDs)
		}
		else if let nativeBeacon = nativeBeacon, let openID = openID {
			request2 = ValidateRequest(delegate: delegate, nativeBeacon: nativeBeacon,
				openID: openID, regionIDs: regionIDs)
		}

		request2?.originalRequest = self

		if request2 == nil {
			assertionFailure("The request is invalid!")
			delegate.validateRequestDidFinish(self, error: nil)
		}
	}

	override func requestDidFinishWithResponseArray(_ array: [Any], error: Error?) {
		weak var wself = self

		if let error = error,
			(error as NSError).domain == FTMError.domain,
			(error as NSError).code == FTMError.authFailed.rawValue,
			let credentials = FTMSession.recentCredentials,
			originalRequest == nil
		{
			// The request failed due to an auth failure. This should be rare, but if it
			// happens, sign in using the existing in-memory credentials, then try the validate
			// request one more time in case the sign in is able to refresh the auth token.

			FTMSession.signIn(
				appKey: credentials.appKey,
				appSecret: credentials.appSecret,
				username: credentials.username,
				completion: {
					(error: Error?) in
					wself?.makeSecondRequest()
				}
			)

			return
		}
		else if array.count == 1,
			let dict = array[0] as? [String: Any],
			let id = dict["id"] as? String,
			!id.isEmpty,
			let isUnauthorized = dict["unauthorized"] as? Bool,
			let macAddress = dict["mac"] as? String,
			let name = dict["name"] as? String
		{
			// An unauthorized beacon is one that belongs to another company. The SDK hides these
			// from the app that integrates with the SDK, but makes pulse calls in order to provide
			// the server with more information about the environment.

			if isUnauthorized || !macAddress.isEmpty {
				let rssiConstant = dict["rssiConstant"] as? Double ?? -59
				response = (
					attributes: dict["attributes"] as? [String: String] ?? [:],
					id: id,
					isUnauthorized: isUnauthorized,
					macAddress: macAddress,
					name: name,
					registerHyperRegion: dict["isHyperRegion"] as? Bool ?? false,
					rssiConstant: rssiConstant)
			}
		}

		originalRequest?.response = response
		delegate?.validateRequestDidFinish(originalRequest ?? self, error: error)
	}

}

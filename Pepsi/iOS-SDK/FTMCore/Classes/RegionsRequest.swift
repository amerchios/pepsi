//
//  RegionsRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 10/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

protocol RegionsRequestDelegate: class {
	func regionsRequestDidFinish(_ request: RegionsRequest, error: Error?)
}

class RegionsRequest: WebServiceRequest {

	private weak var delegate: RegionsRequestDelegate?
	private(set) static var didMakeRequest = false

	init(delegate: RegionsRequestDelegate) {
		self.delegate = delegate

		var items = [(name: String, value: String)]()
		for (i, id) in LocationManager.monitoredRegionIDs.enumerated() {
			items.append((name: "r\(i)", value: id))
		}

		var path = "sdk/regions"
		if !items.isEmpty {
			path += "?" + AGKQueryString.string(nameValuePairs: items)
		}

		super.init(path: path, body: WebServiceRequest.commonBodyProperties)
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		if error == nil {
			RegionsRequest.didMakeRequest = true
			Regions.populateFromServerDictionary(dict)
		}
		delegate?.regionsRequestDidFinish(self, error: error)
	}

}

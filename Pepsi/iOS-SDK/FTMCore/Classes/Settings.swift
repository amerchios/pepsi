//
//  Settings.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

private let keyAccessToken = "FTMCore.AccessToken"
private let keyBeaconInfos = "FTMCore.BeaconInfos2"
private let keyHyperRegions = "FTMCore.HyperRegions"
private let keyMonitorSigLocChanges = "FTMCore.MonitorSigLocChanges"
private let keyNearestBeaconModeIsCloud = "FTMCore.NearestBeaconModeIsCloud"
private let keyPrivateRegionIDs = "FTMCore.PrivateRegionIDs"
private let keyRegionTypeToMonitor = "FTMCore.RegionTypeToMonitor"
private let keySecureRegionIDs = "FTMCore.SecureRegionIDs"
private let keySessionInput = "FTMCore.SessionInput"

class Settings {

	private static let defaults = UserDefaults.standard

	class var accessToken: String {
		get {
			return defaults.string(forKey: keyAccessToken) ?? ""
		}
		set {
			defaults.set(newValue, forKey: keyAccessToken)
		}
	}

	class var beaconInfos: [String: (dateOfLastImpression: Date, regionIDs: Set<String>)] {
		get {
			var result = [String: (Date, Set<String>)]()
			if let array = defaults.array(forKey: keyBeaconInfos) as? [[Any]] {
				for info in array where info.count == 3 {
					if let beaconID = info[0] as? String,
						let time = info[1] as? TimeInterval,
						let regionIDs = info[2] as? [String]
					{
						let date = Date(timeIntervalSinceReferenceDate: time)
						result[beaconID] = (date, Set(regionIDs))
					}
				}
			}
			return result
		}
		set {
			var array = [Any]()
			for (beaconID, val) in newValue {
				let time = Int(round(val.dateOfLastImpression.timeIntervalSinceReferenceDate))
				array.append([beaconID, time, Array(val.regionIDs)])
			}
			defaults.set(array, forKey: keyBeaconInfos)
		}
	}

	class var hyperRegions: [(uuid: String, major: Int, minor: Int)] {
		get {
			guard let arrays = defaults.array(forKey: keyHyperRegions) as? [[Any]] else { return [] }
			var results = [(String, Int, Int)]()
			for array in arrays where array.count == 3 {
				if let uuid = array[0] as? String,
					let major = array[1] as? Int,
					let minor = array[2] as? Int
				{
					results.append((uuid, major, minor))
				}
			}
			return results
		}
		set {
			let arrays = newValue.map { [$0.uuid, $0.major, $0.minor] }
			defaults.set(arrays, forKey: keyHyperRegions)
		}
	}

	class var monitorSignificantLocationChanges: Bool {
		get {
			return defaults.bool(forKey: keyMonitorSigLocChanges)
		}
		set {
			defaults.set(newValue, forKey: keyMonitorSigLocChanges)
		}
	}

	class var nearestBeaconModeIsCloud: Bool {
		get {
			return defaults.bool(forKey: keyNearestBeaconModeIsCloud)
		}
		set {
			defaults.set(newValue, forKey: keyNearestBeaconModeIsCloud)
		}
	}

	class var privateRegionIDs: [String] {
		get {
			return defaults.stringArray(forKey: keyPrivateRegionIDs) ?? []
		}
		set {
			defaults.set(newValue, forKey: keyPrivateRegionIDs)
		}
	}

	class var regionTypeToMonitor: RegionType {
		get {
			let i = defaults.integer(forKey: keyRegionTypeToMonitor)
			return RegionType(rawValue: i) ?? .private
		}
		set {
			defaults.set(newValue.rawValue, forKey: keyRegionTypeToMonitor)
		}
	}

	class var secureRegionIDs: [String] {
		get {
			return defaults.stringArray(forKey: keySecureRegionIDs) ?? []
		}
		set {
			defaults.set(newValue, forKey: keySecureRegionIDs)
		}
	}

	class var sessionInput: (hash: String, date: Date)? {
		get {
			let dict = defaults.dictionary(forKey: keySessionInput)
			if let dict = dict, let s = dict["s"] as? String, let t = dict["d"] as? TimeInterval {
				return (s, Date(timeIntervalSinceReferenceDate: t))
			}
			return nil
		}
		set {
			let dict: [String: Any]
			if let newValue = newValue {
				dict = ["s": newValue.hash, "d": Int(newValue.date.timeIntervalSinceReferenceDate)]
			}
			else {
				dict = [:]
			}
			defaults.set(dict, forKey: keySessionInput)
		}
	}

}

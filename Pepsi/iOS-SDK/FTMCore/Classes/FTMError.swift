//
//  FTMError.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/// An enumeration of error codes associated with errors returned by the framework.
@objc
public enum FTMError: Int {

	/// The app key or secret is missing from
	/// `FTMSession.signIn(appKey:appSecret:username:completion:)`
	case appKeyOrSecretMissing

	/// Authorization failed during a network request.
	case authFailed

	/// The required `NSBluetoothPeripheralUsageDescription` key is missing from the app's
	/// plist file.
	case bluetoothPeripheralUsageDescriptionMissing

	/// The required `NSLocationAlwaysAndWhenInUseUsageDescription` key is missing from the app’s
	/// plist file.
	case locationAlwaysAndWhenInUseUsageDescriptionMissing

	/// The required `NSLocationAlwaysUsageDescription` key is missing from the app’s plist file.
	case locationAlwaysUsageDescriptionMissing

	/// The use of location services has not been granted.
	case locationServicesDenied

	/// Location services are disabled.
	case locationServicesDisabled

	/// The required `NSLocationWhenInUseUsageDescription` key is missing from the app’s plist file.
	case locationWhenInUseUsageDescriptionMissing

	/// A parameter has an invalid value.
	case parameter

	/// A miscellaneous error occurred while making a network request.
	case webServiceGeneric

	/// The error domain associated with the error codes in this enumeration.
	public static let domain = "FTMCore"

	var error: Error {
		let s: String

		switch self {
			case .appKeyOrSecretMissing:
				s = "The app key or secret is missing."
			case .authFailed:
				s = "Authorization failed."
			case .bluetoothPeripheralUsageDescriptionMissing:
				s = "The required NSBluetoothPeripheralUsageDescription key is missing " +
					"from the app’s plist file."
			case .locationAlwaysAndWhenInUseUsageDescriptionMissing:
				s = "The required NSLocationAlwaysAndWhenInUseUsageDescription key is missing " +
					"from the app’s plist file."
			case .locationAlwaysUsageDescriptionMissing:
				s = "The required NSLocationAlwaysUsageDescription key is missing " +
					"from the app’s plist file."
			case .locationServicesDenied:
				s = "The use of location services has not been granted."
			case .locationServicesDisabled:
				s = "Location services are disabled."
			case .locationWhenInUseUsageDescriptionMissing:
				s = "The required NSLocationWhenInUseUsageDescription key is missing " +
					"from the app’s plist file."
			case .parameter:
				s = "A parameter has an invalid value."
			case .webServiceGeneric:
				s = "Something unexpected happened while making a network request. Please try again."
		}

		return NSError(domain: FTMError.domain, code: rawValue,
			userInfo: [NSLocalizedDescriptionKey: s])
	}

}

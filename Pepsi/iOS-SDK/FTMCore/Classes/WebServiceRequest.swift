//
//  WebServiceRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation

class WebServiceRequest: AGKNetRequestDelegate, Hashable {

	enum ExpectedResponseFormat {
		case array, data, dictionary
	}

	var expectedResponseFormat = ExpectedResponseFormat.dictionary
	private(set) weak var handle: FTMRequestHandle?
	private var netRequest: AGKNetRequest?

	class var commonBodyProperties: [String: Any] {
		let locationIsAuthorized: Bool
		let locationStatus = CLLocationManager.authorizationStatus()
		let locationStatusString: String

		switch locationStatus {
			case .authorizedAlways:
				locationIsAuthorized = true
				locationStatusString = "always"
			case .authorizedWhenInUse:
				locationIsAuthorized = true
				locationStatusString = "when in use"
			case .denied:
				locationIsAuthorized = false
				locationStatusString = "denied"
			case .notDetermined:
				locationIsAuthorized = false
				locationStatusString = "not determined"
			case .restricted:
				locationIsAuthorized = false
				locationStatusString = "restricted"
		}

		var body: [String: Any] = [
			"bluetoothOn": CentralManager.state == .poweredOn,
			"deviceModel": UIDevice.current.model,
			"display": "{\(UIScreen.main.bounds.width),\(UIScreen.main.bounds.height)}",
			"locationSvcOn": locationIsAuthorized,
			"locationSvcState": locationStatusString,
			"os": "ios",
			"osVersion": UIDevice.current.systemVersion,
			"sdkVersion": "FMv" + Constants.frameworkVersion,
		]

		body = addLocation(body: body)
		return body
	}

	var hashValue: Int {
		return ObjectIdentifier(self).hashValue
	}

	init(demoLogMessages: String) {
		handle = FTMRequestHandle(request: self)
		let url = URL(string: "https://demo.footmarks.com/demolog")!

		if !Settings.accessToken.isEmpty {
			if Constants.logNetwork {
				NSLog("%@", "Request: \(url)")
			}

			var headers = [
				"Authorization": "Bearer \(Settings.accessToken)",
			]

			let data = demoLogMessages.data(using: .utf8)!
			headers["Content-Length"] = "\(data.count)"
			headers["Content-Type"] = "text/plain; charset=utf-8"

			if Constants.logNetwork {
				if let s = String(data: data, encoding: .utf8) {
					NSLog("%@", "Request body: \(s)")
				}
			}

			netRequest = AGKNetRequest(
				delegate: self,
				url: url,
				ignoreInteraction: false,
				showNetActivityIndicator: true,
				headers: headers,
				body: data,
				method: .post,
				writeResponseToFile: false)
		}
		else {
			DispatchQueue.main.async {
				self.requestDidFailAuth()
			}
		}
	}

	init(path: String, body: [String: Any]?, method: AGKNetRequest.Method? = nil) {
		if path.isEmpty {
			assertionFailure("The web service request path is missing!")
		}

		handle = FTMRequestHandle(request: self)
		let url: URL

		if path == "VERIFY_ACCESS" {
			url = Constants.urlVerifyAccess
		}
		else {
			url = URL(string: Constants.urlWebService.absoluteString + path)!
		}

		if !Settings.accessToken.isEmpty {
			if Constants.logNetwork {
				NSLog("%@", "Request: \(url)")
			}

			var headers = [
				"Authorization": "Bearer \(Settings.accessToken)",
			]

			var bodyData: Data?

			if let body = body {
				let data = try! JSONSerialization.data(withJSONObject: body, options: [])
				bodyData = data
				headers["Content-Length"] = "\(data.count)"
				headers["Content-Type"] = "application/json"

				if Constants.logNetwork {
					if let s = String(data: data, encoding: .utf8) {
						NSLog("%@", "Request body: \(s)")
					}
				}
			}

			netRequest = AGKNetRequest(
				delegate: self,
				url: url,
				ignoreInteraction: false,
				showNetActivityIndicator: true,
				headers: headers,
				body: bodyData,
				method: method ?? (bodyData == nil ? .get : .post),
				writeResponseToFile: false)
		}
		else {
			DispatchQueue.main.async {
				self.requestDidFailAuth()
			}
		}
	}

	init(signInWithAppKey appKey: String, appSecret: String, username: String?) {
		var body = WebServiceRequest.commonBodyProperties

		body["appVersion"] = Constants.frameworkVersion + " (" + Constants.frameworkBuild + ")"
		body["grant_type"] = "app_credentials"
		body["username"] = username ?? ""

		let (id, isIDFA) = Device.id
		if isIDFA {
			body["advertisingId"] = id
		}
		else {
			body["deviceId"] = id
		}

		handle = FTMRequestHandle(request: self)
		let url = Constants.urlSignIn

		if Constants.logNetwork {
			NSLog("%@", "Request: \(url)")
		}

		let authData = (appKey + ":" + appSecret).data(using: .utf8)
		let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])

		if Constants.logNetwork {
			if let s = String(data: bodyData, encoding: .utf8) {
				NSLog("%@", "Request body: \(s)")
			}
		}

		netRequest = AGKNetRequest(
			delegate: self,
			url: url,
			ignoreInteraction: false,
			showNetActivityIndicator: true,
			headers: [
				"Authorization": "Basic \(authData?.base64EncodedString() ?? "ERROR")",
				"Content-Length": "\(bodyData.count)",
				"Content-Type": "application/json",
			],
			body: bodyData,
			method: .post,
			writeResponseToFile: false)
	}

	static func ==(lhs: WebServiceRequest, rhs: WebServiceRequest) -> Bool {
		return lhs === rhs
	}

	class func addLocation(body: [String: Any],
		location: (location: CLLocation, date: Date)? = nil) -> [String: Any]
	{
		var body = body
		if let loc = (location ?? LocationManager.recentLocation) {
			body["horizontalAccuracy"] = loc.location.horizontalAccuracy
			body["lat"] = loc.location.coordinate.latitude
			body["locTimestamp"] = Int64(loc.date.timeIntervalSince1970)
			body["lon"] = loc.location.coordinate.longitude
		}
		return body
	}

	func netRequestDidFinish(_ netRequest: AGKNetRequest, error: Error?) {
		defer {
			self.handle?.cancel()
			self.handle = nil
			self.netRequest = nil
		}

		if Constants.logNetwork {
			let s0 = String(data: netRequest.responseBody, encoding: .utf8)
			let s1 = "Response (\(netRequest.statusCode)): " + (s0 ?? "(no body)")
			NSLog("%@", s1)
		}

		var error = error
		let goodCodes: Set<Int> = [200, 201, 401]

		if error == nil && !goodCodes.contains(netRequest.statusCode) {
			error = FTMError.webServiceGeneric.error
		}

		if netRequest.statusCode == 401 {
			// The auth token has most likely expired.
			requestDidFailAuth()
			return
		}

		switch expectedResponseFormat {
			case .array:
				var responseArray = [Any]()
				if error == nil {
					let obj = try? JSONSerialization.jsonObject(
						with: netRequest.responseBody, options: [])
					if let array = obj as? [Any] {
						responseArray = array
					}
				}
				requestDidFinishWithResponseArray(responseArray, error: error)

			case .data:
				var contentType: String?
				if error == nil {
					for (key, val) in netRequest.responseHeaders {
						if key.lowercased() == "content-type" {
							contentType = val
							break
						}
					}
					if contentType?.lowercased() != "application/octet-stream" {
						error = FTMError.webServiceGeneric.error
					}
				}
				requestDidFinishWithResponseData(error == nil ? netRequest.responseBody : Data(),
					error: error)

			case .dictionary:
				var responseDict = [String: Any]()
				if error == nil {
					let obj = try? JSONSerialization.jsonObject(
						with: netRequest.responseBody, options: [])
					if let dict = obj as? [String: Any] {
						responseDict = dict
					}
				}
				requestDidFinishWithResponseDict(responseDict, error: error)
		}
	}

	private func requestDidFailAuth() {
		switch expectedResponseFormat {
			case .array:
				requestDidFinishWithResponseArray([], error: FTMError.authFailed.error)
			case .data:
				requestDidFinishWithResponseData(Data(), error: FTMError.authFailed.error)
			case .dictionary:
				requestDidFinishWithResponseDict([:], error: FTMError.authFailed.error)
		}
	}

	func requestDidFinishWithResponseArray(_ array: [Any], error: Error?) {
		// Subclasses should override.
	}

	func requestDidFinishWithResponseData(_ data: Data, error: Error?) {
		// Subclasses should override.
	}

	func requestDidFinishWithResponseDict(_ dict: [String: Any], error: Error?) {
		// Subclasses should override.
	}

}

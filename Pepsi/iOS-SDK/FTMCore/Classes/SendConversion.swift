//
//  SendConversion.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/25/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

class SendConversion: WebServiceRequest {

	private let completion: ((Error?) -> Void)?

	init(action: String?,
		customActionName: String,
		customValueName: String,
		eventID: String,
		value: String,
		valueType: FTMExperienceValueType?,
		completion: ((Error?) -> Void)?)
	{
		self.completion = completion

		super.init(path: "eventresponseresults", body: [
			"converted": true,
			"convertedType": action ?? "custom",
			"convertedTypeCustomName": customActionName,
			"convertedValue": value,
			"convertedValueCustomName": customValueName,
			"convertedValueType": valueType?.string ?? "custom",
			"event": eventID,
		])
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		completion?(error)
	}

}

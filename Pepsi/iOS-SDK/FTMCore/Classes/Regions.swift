//
//  Regions.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/3/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation

class Regions {

	private(set) static var beaconRegions = [CLBeaconRegion]()
	private(set) static var circularRegions = [CLCircularRegion]()
	private static var didPopulateHyperRegionsFromSettings = false
	static let eventDidUpdate = Notification.Name("FTMCore.RegionsDidUpdate")
	private(set) static var hyperRegions = [CLBeaconRegion]()
	private static var knownCircularRegionIDs = Set<String>()

	class func addHyperRegion(proximityUUID: UUID, major: Int, minor: Int) {
		populateHyperRegionsFromSettingsIfNeeded()

		var found = false
		for r in hyperRegions where
			r.proximityUUID == proximityUUID &&
			r.major?.intValue == major &&
			r.minor?.intValue == minor
		{
			found = true
			break
		}

		if found {
			if Constants.logLocationManager {
				let id = hyperRegionID(major: major, minor: minor)
				NSLog("Region \(id) will not be added because it already exists.")
			}
		}
		else {
			let region = CLBeaconRegion(
				proximityUUID: proximityUUID,
				major: CLBeaconMajorValue(major),
				minor: CLBeaconMinorValue(minor),
				identifier: hyperRegionID(major: major, minor: minor))
			hyperRegions.insert(region, at: 0)
			if Constants.logLocationManager {
				NSLog("Region \(region.identifier) has been added.")
			}
		}

		var regionCount = beaconRegions.count + circularRegions.count + hyperRegions.count
		while regionCount > 20 && !hyperRegions.isEmpty {
			let region = hyperRegions.removeLast()
			if Constants.logLocationManager {
				NSLog("Too many regions. Region \(region.identifier) has been removed.")
			}
			regionCount -= 1
		}

		Settings.hyperRegions = hyperRegions.map {
			($0.proximityUUID.uuidString, $0.major!.intValue, $0.minor!.intValue)
		}

		notify()
	}

	private class func hyperRegionID(major: Int, minor: Int) -> String {
		return Constants.regionIDPrefix + "-hyper:\(major),\(minor)"
	}

	class func isKnown(circularRegion: CLCircularRegion) -> Bool {
		return knownCircularRegionIDs.contains(circularRegion.identifier)
	}

	private class func notify() {
		NotificationCenter.default.post(name: eventDidUpdate, object: self)
	}

	class func populateFromServerDictionary(_ dictionary: [String: Any]) {
		Settings.privateRegionIDs = dictionary["privateRegions"] as? [String] ?? []
		Settings.secureRegionIDs = dictionary["secureRegions"] as? [String] ?? []

		let regionType = dictionary["regionType"] as? String ?? ""
		Settings.regionTypeToMonitor = RegionType(serverValue: regionType) ?? .private

		circularRegions.removeAll()

		// Don't remove all from knownCircularRegionIDs here. That list should grow indefinintely
		// so that the location manager recognizes region IDs even when the server updates them.
		// That way, the region enter/exit events aren't incorrectly filtered out.

		for dict in dictionary["geofenceRegions"] as? [[String: Any]] ?? [] {
			guard
				let id = dict["id"] as? String,
				let lat = dict["lat"] as? Double,
				let lon = dict["lon"] as? Double,
				let radius = dict["radius"] as? Double
			else {
				assertionFailure("The geofence region is invalid!")
				continue
			}
			let center = CLLocationCoordinate2D(latitude: lat, longitude: lon)
			let region = CLCircularRegion(center: center, radius: radius, identifier: id)
			circularRegions.append(region)
			knownCircularRegionIDs.insert(region.identifier)
		}

		populateFromSettings()
	}

	class func populateFromSettings() {
		populateHyperRegionsFromSettingsIfNeeded()

		let ids: [String]
		let prefix: String

		switch Settings.regionTypeToMonitor {
			case .private:
				ids = Settings.privateRegionIDs
				prefix = Constants.regionIDPrefix + "-private"
			case .secure:
				ids = Settings.secureRegionIDs
				prefix = Constants.regionIDPrefix + "-secure"
				if !Constants.hasBackgroundModeBluetoothCentral {
					NSLog("WARNING: Payment regions are being monitoried, but UIBackgroundModes " +
						"doesn't have an entry for bluetooth-central, which will prevent some " +
						"functionality from working.")
				}
		}

		beaconRegions.removeAll()

		for (i, id) in ids.enumerated() {
			let components = id.components(separatedBy: ",")
			let numericPart = String(format: "%02d", i + 1)

			guard !components.isEmpty else {
				assertionFailure("The region ID has no components!")
				continue
			}

			guard let uuid = UUID(uuidString: components[0]) else {
				assertionFailure("The region UUID is invalid!")
				continue
			}

			var major: CLBeaconMajorValue?
			var minor: CLBeaconMinorValue?

			if components.count > 1 {
				major = CLBeaconMajorValue(components[1])
				guard major != nil else {
					assertionFailure("The region's major is invalid!")
					continue
				}
				if components.count > 2 {
					minor = CLBeaconMinorValue(components[2])
					guard minor != nil else {
						assertionFailure("The region's minor is invalid!")
						continue
					}
				}
			}

			if let major = major {
				if let minor = minor {
					beaconRegions.append(CLBeaconRegion(proximityUUID: uuid, major: major,
						minor: minor, identifier: prefix + numericPart + ":\(major),\(minor)"))
				}
				else {
					beaconRegions.append(CLBeaconRegion(proximityUUID: uuid, major: major,
						identifier: prefix + numericPart + ":\(major)"))
				}
			}
			else {
				beaconRegions.append(CLBeaconRegion(proximityUUID: uuid,
					identifier: prefix + numericPart))
			}
		}

		notify()
	}

	private class func populateHyperRegionsFromSettingsIfNeeded() {
		guard !didPopulateHyperRegionsFromSettings else { return }
		didPopulateHyperRegionsFromSettings = true

		assert(hyperRegions.isEmpty)
		hyperRegions.removeAll()

		for item in Settings.hyperRegions {
			guard let uuid = UUID(uuidString: item.uuid) else {
				assertionFailure("The hyper region UUID is invalid!")
				continue
			}
			let id = hyperRegionID(major: item.major, minor: item.minor)
			hyperRegions.append(CLBeaconRegion(
				proximityUUID: uuid,
				major: CLBeaconMajorValue(item.major),
				minor: CLBeaconMinorValue(item.minor),
				identifier: id))
			if Constants.logLocationManager {
				NSLog("Region \(id) has been read from settings.")
			}
		}
	}

	class func removeHyperRegion(_ region: CLBeaconRegion) {
		populateHyperRegionsFromSettingsIfNeeded()
		for (i, r) in hyperRegions.enumerated() where r.identifier == region.identifier {
			hyperRegions.remove(at: i)
			Settings.hyperRegions = hyperRegions.map {
				($0.proximityUUID.uuidString, $0.major!.intValue, $0.minor!.intValue)
			}
			if Constants.logLocationManager {
				NSLog("Region \(region.identifier) has been removed.")
			}
			notify()
			break
		}
	}

}

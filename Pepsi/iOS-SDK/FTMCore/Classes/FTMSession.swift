//
//  FTMSession.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/// A class for managing a session with the SmartConnect Cloud.
public class FTMSession: NSObject {

	static var isSignedInPrivate = false
	static var recentCredentials: (appKey: String, appSecret: String, username: String?)?

	/// Whether or not a session is active.
	///
	/// After a successful call to `signIn(appKey:appSecret:username:completion:)`,
	/// a session will be established and this property will return `true`.
	@objc
	public class var isSignedIn: Bool {
		return isSignedInPrivate && !Settings.accessToken.isEmpty
	}

	/**
		Deletes the data associated with the current session from the SmartConnect Cloud,
		then signs out.

		If the request completes without error, there will no longer be an active session, and
		beacon management will come to a halt. To start managing beacons again, call
		`signIn(appKey:appSecret:username:completion:)`

		- parameters:
			- completion: The completion handler.
		- returns:
			A request handle.
	*/
	@discardableResult
	@objc
	public class func deleteSessionAndUserData(completion: ((Error?) -> Void)?) -> FTMRequestHandle {
		return FTMSessionDeleteUserDataRequest(completion: completion).handle!
	}

	class func doPostAuthProcessing(hash: String) {
		Settings.sessionInput = (hash, Date())
		FTMSession.isSignedInPrivate = true

		// Let one run loop pass before starting the managers. This causes the respective auth
		// completion handler to be called first, giving the caller a chance to set delegates and
		// not miss out on any activity.

		DispatchQueue.main.async {
			CentralManager.isPaused = false
			LocationManager.isPaused = false
			DemoLogger.go()
			FTMBeaconManager.go()
		}

		// Wait a bit before making the device info request. This gives the user a chance to
		// accept location manager and bluetooth permissions, giving the request a better chance
		// of reporting accurate state information.

		DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
			_ = DeviceInfoRequest()
		}
	}

	/**
		Signs in to establish a session with the SmartConnect Cloud.

		- parameters:
			- appKey: The app key.
			- appSecret: The app secret.
			- username: An optional username to associate with the session. This can be any
				arbitrary identifier.
			- completion: The completion handler.
		- returns:
			A request handle.
	*/
	@discardableResult
	@objc(signInWithAppKey:appSecret:username:completion:)
	public class func signIn(
		appKey: String,
		appSecret: String,
		username: String?,
		completion: @escaping (Error?) -> Void) -> FTMRequestHandle
	{
		FTMSession.recentCredentials = (appKey, appSecret, username)

		var username = username
		if username == "__TurnOffIDFA__" {
			// This is a private API that isn't an official part of the SDK. It ensures that the
			// device ID is not based on the IDFA.
			Device.allowIDFA = false
			username = nil
		}

		guard !appKey.isEmpty && !appSecret.isEmpty else {
			DispatchQueue.main.async {
				completion(FTMError.appKeyOrSecretMissing.error)
			}
			return FTMRequestHandle()
		}

		guard Constants.hasBluetoothPeripheralUsageDescription else {
			DispatchQueue.main.async {
				completion(FTMError.bluetoothPeripheralUsageDescriptionMissing.error)
			}
			return FTMRequestHandle()
		}

		guard Constants.hasLocationAlwaysAndWhenInUseUsageDescription else {
			DispatchQueue.main.async {
				completion(FTMError.locationAlwaysAndWhenInUseUsageDescriptionMissing.error)
			}
			return FTMRequestHandle()
		}

		guard Constants.hasLocationAlwaysUsageDescription else {
			DispatchQueue.main.async {
				completion(FTMError.locationAlwaysUsageDescriptionMissing.error)
			}
			return FTMRequestHandle()
		}

		guard Constants.hasLocationWhenInUseUsageDescription else {
			DispatchQueue.main.async {
				completion(FTMError.locationWhenInUseUsageDescriptionMissing.error)
			}
			return FTMRequestHandle()
		}

		// If we can, try the verify endpoint rather than sign in. Verify is cheaper on the server.
		// Verification is attempted when the app key, app secret, and username match the
		// previously successful sign in, and when it has been 24 hours or less since the last
		// successful sign in. If more than 24 hours, verification is more likely to fail, so
		// rather than waste the time on a failed attempt, a sign in is attempted instead.

		let hash = FTMSessionVerifyRequest.hash(appKey: appKey, appSecret: appSecret,
			username: username)

		if let sessionInput = Settings.sessionInput,
			sessionInput.hash == hash,
			sessionInput.date.timeIntervalSinceNow >= -TimeInterval(24 * 3600)
		{
			let request = FTMSessionVerifyRequest(hash: hash) {
				(error: Error?) in
				if error == nil {
					completion(nil)
				}
				else {

					// Verification failed. Go ahead and try to sign in. Note that a new
					// request is created here, but the caller doesn't have access to its
					// handle. This isn't a huge deal, in part because getting here should be a
					// very rare occurrence. It just means that for this second phase of the
					// request, the caller can't cancel it.

					_ = FTMSessionSignInRequest(
						appKey: appKey,
						appSecret: appSecret,
						username: username,
						completion: completion)
				}
			}
			return request.handle!
		}

		// Verification isn't possible. Go ahead and try to sign in.

		let request = FTMSessionSignInRequest(
			appKey: appKey,
			appSecret: appSecret,
			username: username,
			completion: completion)

		return request.handle!
	}

	/// Signs out the current session.
	///
	/// Signing out causes beacon management to come to a halt. To start managing beacons again,
	/// call `signIn(appKey:appSecret:username:completion:)`
	@objc
	public class func signOut() {
		isSignedInPrivate = false
		recentCredentials = nil
		Settings.accessToken = ""
		Settings.sessionInput = nil
		CentralManager.isPaused = true
		LocationManager.isPaused = true
	}

}

private class FTMSessionDeleteUserDataRequest: WebServiceRequest {

	private let completion: ((Error?) -> Void)?

	init(completion: ((Error?) -> Void)?) {
		self.completion = completion
		super.init(path: "self/remove", body: nil, method: .post)
	}

	override func requestDidFinishWithResponseDict(_ dict: [String: Any], error: Error?) {
		if error == nil {
			FTMSession.signOut()
		}
		completion?(error)
	}

}

private class FTMSessionSignInRequest: WebServiceRequest {

	private let appKey: String
	private let appSecret: String
	private let completion: (Error?) -> Void
	private let username: String?

	init(appKey: String, appSecret: String, username: String?,
		completion: @escaping (Error?) -> Void)
	{
		self.appKey = appKey
		self.appSecret = appSecret
		self.completion = completion
		self.username = username
		super.init(signInWithAppKey: appKey, appSecret: appSecret, username: username)
	}

	override func requestDidFinishWithResponseDict(_ dict: [String: Any], error: Error?) {
		var error = error

		if let token = dict["accessToken"] as? String, !token.isEmpty {
			Settings.accessToken = token
			Settings.monitorSignificantLocationChanges = dict["locChangeActive"] as? Bool ?? false
			Settings.nearestBeaconModeIsCloud = (dict["nearestMode"] as? String == "cloud")

			if Constants.logNetwork {
				NSLog("%@", "Nearest beacon cloud mode = \(Settings.nearestBeaconModeIsCloud).")
			}

			Regions.populateFromServerDictionary(dict)
			FTMSession.doPostAuthProcessing(hash: FTMSessionVerifyRequest.hash(appKey: appKey,
				appSecret: appSecret, username: username))
		}
		else if error == nil {
			error = FTMError.authFailed.error
		}

		completion(error)
	}

}

private class FTMSessionVerifyRequest: WebServiceRequest {

	private let completion: (Error?) -> Void
	private let hash: String

	init(hash: String, completion: @escaping (Error?) -> Void) {
		self.completion = completion
		self.hash = hash
		super.init(path: "VERIFY_ACCESS", body: nil)
	}

	class func hash(appKey: String, appSecret: String, username: String?) -> String {
		let input = appKey + appSecret + (username ?? "")
		let data = __FTMPrivate.__a(100, __b: input.data(using: .utf8)) as! Data
		return AGKHex.string(data: data)
	}

	override func requestDidFinishWithResponseDict(_ dict: [String: Any], error: Error?) {
		if error == nil {
			Regions.populateFromSettings()
			FTMSession.doPostAuthProcessing(hash: hash)
		}
		completion(error)
	}

}

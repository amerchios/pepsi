//
//  DeviceInfoRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/3/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

class DeviceInfoRequest: WebServiceRequest {

	init() {
		let body = WebServiceRequest.commonBodyProperties
		super.init(path: "userdevices/000000000000000000000000", body: body, method: .put)
	}

}

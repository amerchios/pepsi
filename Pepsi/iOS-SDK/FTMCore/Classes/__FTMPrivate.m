//
//  __FTMPrivate.m
//  FTMCore
//
//  Created by Shane Meyer on 5/12/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//
//  This class exists to work around the fact that CommonCrypto and Swift don't play nicely
//  together inside a framework.
//

#import "__FTMPrivate.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>

static NSString *logMessagesAsString;

//
// __FTMPrivateLog
//

@interface __FTMPrivateLog : NSObject

@end

@implementation __FTMPrivateLog

- (instancetype)init {
	if (self = [super init]) {
		NSNotificationCenter *nc = NSNotificationCenter.defaultCenter;
		[nc addObserver:self selector:@selector(onGetLogMessagesAsString1:)
			name:@"__FTMPrivateGetLogMessagesAsString1" object:nil];
	}
	return self;
}

- (void)dealloc {
	[NSNotificationCenter.defaultCenter removeObserver:self];
}

- (void)onGetLogMessagesAsString1:(NSNotification *)notification {
	logMessagesAsString = notification.userInfo[@"messages"];
}

@end

//
// __FTMPrivate
//

@implementation __FTMPrivate

+ (nullable id)__a:(NSInteger)__a __b:(nullable id)__b {
	NSNotificationCenter *nc = NSNotificationCenter.defaultCenter;

	if (__a == 100) {
		return (__b == nil) ? nil : [self sha1:__b];
	}
	else if (__a == 200) {
		NSString *majorMinor = __b;
		if (majorMinor == nil) {
			return nil;
		}
		NSArray<NSString *> *comps = [majorMinor componentsSeparatedByString:@":"];
		if (comps.count != 2) {
			return nil;
		}
		uint16_t major = comps[0].integerValue;
		uint16_t minor = comps[1].integerValue;
		return [self peripheralServiceIDForMajor:major minor:minor];
	}

	// Demo logging. These two cases use the notification center to move data between this class
	// and the private DemoLogger Swift class. It's a little convoluted but keeps things as private
	// as possible.

	else if (__a == 300) {
		NSString *message = __b;
		if (message != nil) {
			[nc postNotificationName:@"__FTMPrivateLogMessage"
				object:message userInfo:@{ @"message" : message }];
		}
	}
	else if (__a == 400) {
		logMessagesAsString = nil;
		__FTMPrivateLog *helper = [[__FTMPrivateLog alloc] init];
		[helper description];
		[nc postNotificationName:@"__FTMPrivateGetLogMessagesAsString0" object:nil];
		return logMessagesAsString;
	}

	return nil;
}

//
// Returns the peripheral service ID for a secure broadcast.
//

+ (NSData *)peripheralServiceIDForMajor:(uint16_t)major minor:(uint16_t)minor {
	uint8_t b0 = major >> 8;
	uint8_t b1 = major & 0xFF;
	uint8_t b2 = minor >> 8;
	uint8_t b3 = minor & 0xFF;

	uint8_t dataIn[] = {
		b0, b1, b2, b3,
		b0, b1, b2, b3,
		b0, b1, b2, b3,
		b0, b1, b2, b3,
	};

	uint8_t key[] = {
		0xbb, 0xbb, 0xbb, 0xbb,
		0xcc, 0xcc, 0xcc, 0xcc,
		0xbb, 0xbb, 0xbb, 0xbb,
		0xcc, 0xcc, 0xcc, 0xcc,
	};

	const size_t dataOutAvailable = sizeof(dataIn) + kCCBlockSizeAES128;
	uint8_t dataOut[dataOutAvailable] = {};
	size_t numBytesEncrypted = 0;

	CCCrypt(kCCEncrypt,
		kCCAlgorithmAES128,
		kCCOptionECBMode,
		key,
		sizeof(key),
		NULL,
		dataIn,
		sizeof(dataIn),
		dataOut,
		dataOutAvailable,
		&numBytesEncrypted);

	return [NSData dataWithBytes:dataOut length:numBytesEncrypted];
}

//
// Returns a SHA1 hash of the given data.
//

+ (NSData *)sha1:(NSData *)dataIn {
	NSMutableData *dataOut = [[NSMutableData alloc] initWithLength:CC_SHA1_DIGEST_LENGTH];
	CC_SHA1(dataIn.bytes, (CC_LONG)dataIn.length, dataOut.mutableBytes);
	return dataOut;
}

@end

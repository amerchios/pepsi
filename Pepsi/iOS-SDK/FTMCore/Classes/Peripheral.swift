//
//  Peripheral.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/12/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

class Peripheral: Hashable {

	private(set) var batteryLevel = 0.0
	private(set) var clockHour = 0
	private var dateOfLastImpression = Date()
	private(set) var firmware = 0
	let macAddress: String
	private(set) var radius = 0
	private(set) var rssi = 0.0
	private(set) var serviceID: String

	var hashValue: Int {
		return macAddress.hashValue ^ serviceID.hashValue
	}

	var isExpired: Bool {
		return dateOfLastImpression.timeIntervalSinceNow < -300.0
	}

	init?(byte0: UInt8, byte1: UInt8, macAddress: String, rssi: Double, serviceID: String) {
		self.macAddress = macAddress
		self.serviceID = serviceID
		if byte0 == 90 && byte1 == 90 {
			// The byte pattern 0x5A5A represents pre V0 firmware. We don't support this or V0.
			return nil
		}
		updateValues(byte0: byte0, byte1: byte1, rssi: rssi, serviceID: serviceID)
	}

	static func ==(lhs: Peripheral, rhs: Peripheral) -> Bool {
		return
			lhs.batteryLevel == rhs.batteryLevel &&
			lhs.clockHour == rhs.clockHour &&
			lhs.dateOfLastImpression == rhs.dateOfLastImpression &&
			lhs.firmware == rhs.firmware &&
			lhs.macAddress == rhs.macAddress &&
			lhs.radius == rhs.radius &&
			lhs.rssi == rhs.rssi &&
			lhs.serviceID == rhs.serviceID
	}

	func updateValues(byte0: UInt8, byte1: UInt8, rssi: Double, serviceID: String) {
		self.rssi = rssi
		self.serviceID = serviceID
		dateOfLastImpression = Date()

		//
		// Bits 0-3 are the zero-based radius related to the Tx power level.
		//   0000 = -30 dBm
		//   0001 = -20 dBm
		//   ...
		//   0111 =   4 dBm
		//
		// Bits 4-7 are the battery level in 10% increments.
		//   0000 =   0%
		//   0001 =  10%
		//   0010 =  20%
		//   ...
		//   1010 = 100%
		//
		// Bits 8-10 are the firmware version.
		//   000 = 0
		//   001 = 1
		//   ...
		//
		// Bits 11-15 are the clock in 1 hour increments.
		//   00000 = 12am
		//   00001 =  1am
		//   00010 =  2am
		//   10111 = 11pm
		//

		batteryLevel = Double(max(0, min(10, Int(byte0 & 0xF)))) / 10.0
		clockHour = Int(byte1 & 0x1F)
		firmware = Int(byte1 >> 5)
		radius = Int(byte0 >> 4) + 1
	}

}

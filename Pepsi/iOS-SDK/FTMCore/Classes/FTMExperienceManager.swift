//
//  FTMExperienceManager.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/30/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/// A delegate for communicating experience related activity.
@objc
public protocol FTMExperienceManagerDelegate: class {

	/// Called when one or more experiences are received from the SmartConnect Cloud.
	///
	/// When an experience is received, the app should inspect its properties to take the
	/// appropriate action. It might be the case that the app is in the background when an
	/// experience is received, making it impossible to deliver the expected user interface at that
	/// moment. If that happens, persist the experience for later, taking advantage of the fact that
	/// `FTMExperience` conforms to `NSCoding`. When the app moves to the foreground, the app
	/// can then process its persisted experiences.
	///
	/// Once an experience has been processed and its intended action has been performed, the app
	/// should send an experience conversion to the cloud by calling
	/// `FTMExperience.sendConversion(action:valueType:value:completion:)` or
	/// `FTMExperience.sendConversion(customActionName:customValueName:value:completion:)`
	func experienceManagerDidReceiveExperiences(_ experiences: Set<FTMExperience>)

}

/// The experience manager, which communicates experience related activity to its delegate.
public class FTMExperienceManager: NSObject {

	/// The delegate.
	@objc
	public static weak var delegate: FTMExperienceManagerDelegate?

}

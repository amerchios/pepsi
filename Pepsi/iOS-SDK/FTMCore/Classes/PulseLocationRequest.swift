//
//  PulseLocationRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 10/14/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation

protocol PulseLocationRequestDelegate: class {
	func pulseLocationRequestDidFinish(_ request: PulseLocationRequest, error: Error?)
}

class PulseLocationRequest: WebServiceRequest {

	private weak var delegate: PulseLocationRequestDelegate?
	private(set) var experiences = Set<FTMExperience>()
	private weak var originalRequest: PulseLocationRequest?
	private let location: (location: CLLocation, date: Date)
	private var request2: PulseLocationRequest?

	init(delegate: PulseLocationRequestDelegate, location: (location: CLLocation, date: Date)) {
		self.delegate = delegate
		self.location = location
		var body: [String: Any] = [
			"regionState": "enter",
			"type": "locChange",
		]
		body = WebServiceRequest.addLocation(body: body, location: location)
		super.init(path: "pulse", body: body)
	}

	private func makeSecondRequest() {
		guard let delegate = delegate else { return }
		let request = PulseLocationRequest(delegate: delegate, location: location)
		request2 = request
		request.originalRequest = self
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		weak var wself = self

		if let error = error,
			(error as NSError).domain == FTMError.domain,
			(error as NSError).code == FTMError.authFailed.rawValue,
			let credentials = FTMSession.recentCredentials,
			originalRequest == nil
		{
			// The request failed due to an auth failure. This should be rare, but if it
			// happens, sign in using the existing in-memory credentials, then try the pulse
			// request one more time in case the sign in is able to refresh the auth token.

			FTMSession.signIn(
				appKey: credentials.appKey,
				appSecret: credentials.appSecret,
				username: credentials.username,
				completion: {
					(error: Error?) in
					wself?.makeSecondRequest()
				}
			)

			return
		}

		experiences = PulseBeaconRequest.parseExperiencesFromServerDictionary(dict)
		originalRequest?.experiences = experiences
		delegate?.pulseLocationRequestDidFinish(originalRequest ?? self, error: error)
	}

}

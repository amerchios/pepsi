//
//  FTMBeaconManager.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/4/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreBluetooth
import CoreLocation

/// A delegate for communicating beacon related updates and state changes.
@objc
public protocol FTMBeaconManagerDelegate: class {

	/// Called when the underlying `CLLocationManager` changes its authorization status.
	///
	/// Call `CLLocationManager.authorizationStatus()` to retrieve the status. An app might update
	/// its user interface in response to a status change, for example.
	@objc
	optional func beaconManagerDidChangeLocationManagerAuthorizationStatus()

	/// Called when the beacon manager experiences an error.
	///
	/// If the error's domain matches `FTMError.domain`, the error code can be comapred to one of
	/// the `FTMError` enumeration values.
	func beaconManagerDidFail(error: Error)

	/// Called when the beacon manager updates its beacons.
	///
	/// Beacons are updated when entering a region, when idling within in a region, and when exiting
	/// a region. The latest beacons are available by calling `FTMBeaconManager.beacons`.
	func beaconManagerDidRangeBeacons()

	/// Called when the underlying `CBCentralManager` updates its state.
	///
	/// Call `FTMBeaconManager.centralManagerState` to retrieve the state. An app might update
	/// its user interface in response to a state change, for example.
	@objc
	optional func beaconManagerDidUpdateCentralManagerState()
}

/// The beacon manager, which maintains a list of in-range beacons and communicates state changes
/// with its delegate.
public class FTMBeaconManager: NSObject {

	private var attributeKeyValToBeaconID = [String: String]()
	private var dateWhenAppEnteredForeground = Date()
	fileprivate var macAddressesNotProvisioned = Set<String>()
	fileprivate var openIDsNotProvisioned = Set<String>()
	fileprivate var pulseBeaconRequests = Set<PulseBeaconRequest>()
	fileprivate var pulseCircularRegionRequests = Set<PulseCircularRegionRequest>()
	fileprivate var pulseLocationRequests = Set<PulseLocationRequest>()
	private static let shared = FTMBeaconManager()
	fileprivate var validateRequests = Set<ValidateRequest>()
	fileprivate var validatedBeaconsByServiceID = [String: FTMBeacon]()

	/// All beacons that are in range of the current device.
	///
	/// When entering a region, one or more beacons are added to the set, and when exiting a region
	/// they are removed. Changes can be monitoried by implementing
	/// `FTMBeaconManagerDelegate.beaconManagerDidRangeBeacons()`.
	@objc
	public class var beacons: Set<FTMBeacon> {

		// Return authorized beacons only. Unauthorized beacons belong to another company and
		// therefore should be hidden from the caller.

		return Set(shared.validatedBeaconsByServiceID.values.filter { $0.isUnauthorized == false })
	}

	/// The state of the underlying `CBCentralManager`.
	///
	/// Changes can be monitoried by implementing
	/// `FTMBeaconManagerDelegate.beaconManagerDidUpdateCentralManagerState()`.
	@objc
	public class var centralManagerState: CBCentralManagerState {
		return CentralManager.state
	}

	/// The delegate.
	@objc
	public static weak var delegate: FTMBeaconManagerDelegate?

	private override init() {
		super.init()

		// Start the main processing loop that runs forever. Do this on the next run loop to avoid
		// a situation where process() calls FTMBeaconManagerDelegate.beaconManagerDidRangeBeacons(),
		// which might access our beacons property, which would access the shared property, causing
		// this init() to be called in a loop.

		DispatchQueue.main.async {
			self.process(loop: true)
		}

		let nc = NotificationCenter.default
		nc.addObserver(self, selector: #selector(onAppWillEnterForeground),
			name: .UIApplicationWillEnterForeground, object: nil)
		nc.addObserver(self, selector: #selector(onCentralManagerDidUpdatePeripherals),
			name: CentralManager.eventDidUpdatePeripherals, object: nil)
		nc.addObserver(self, selector: #selector(onCentralManagerDidUpdateState),
			name: CentralManager.eventDidUpdateState, object: nil)
		nc.addObserver(self, selector: #selector(onLocationManagerDidChangeAuthorizationStatus),
			name: LocationManager.eventDidChangeAuthorizationStatus, object: nil)
		nc.addObserver(self, selector: #selector(onLocationManagerDidEnterCircularRegion(_:)),
			name: LocationManager.eventDidEnterCircularRegion, object: nil)
		nc.addObserver(self, selector: #selector(onLocationManagerDidExitBeaconRegion(_:)),
			name: LocationManager.eventDidExitBeaconRegion, object: nil)
		nc.addObserver(self, selector: #selector(onLocationManagerDidExitCircularRegion(_:)),
			name: LocationManager.eventDidExitCircularRegion, object: nil)
		nc.addObserver(self, selector: #selector(onLocationManagerDidFail),
			name: LocationManager.eventDidFail, object: nil)
		nc.addObserver(self, selector: #selector(onLocationManagerDidUpdateBeacons),
			name: LocationManager.eventDidUpdateBeacons, object: nil)
		nc.addObserver(self, selector: #selector(onLocationManagerDidUpdateSignificantLocation),
			name: LocationManager.eventDidUpdateSignificantLocation, object: nil)
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	class func beaconIDMatching(attributeKey: String, attributeValue: String) -> String? {
		return shared.attributeKeyValToBeaconID[attributeKey + "|" + attributeValue]
	}

	private func evaluateBeaconsAndPeripherals() {
		let peripheralsByServiceID = CentralManager.peripheralsByServiceID
		let inBackground = (UIApplication.shared.applicationState == .background)

		for (nativeBeacon, regionIDs) in LocationManager.nativeBeaconsHalfSecondAgo {
			let serviceID = FTMBeacon.peripheralServiceID(nativeBeacon: nativeBeacon)
			let peripheral = peripheralsByServiceID[serviceID]

			if !inBackground || Constants.hasBackgroundModeBluetoothCentral {

				//
				// When the app isn't in the background, or when the bluetooth-central background
				// mode is enabled, the peripheral scanner should have no trouble finding the
				// matching peripheral, if one exists.
				//
				// By iterating over the location manager's native beacons "a half second ago",
				// this ensures that if a matching peripheral exists, it would have been found by
				// now. That's because when iOS encounters a beacon, it nearly simultaneously
				// reports the native beacon to the location manager, and reports the peripheral
				// to the central manager, but the order of those two events is not deterministic.
				// The half second delay solves this.
				//

				if let peripheral = peripheral {
					guard !peripheral.isExpired else { continue }
				}
				else {
					// A matching peripheral was not found. This is an open beacon.
				}
			}

			// Now that the "half second ago" data has served its purpose, make sure that the
			// current beacon is found in the latest, non-delayed location manager data. This is
			// necessary because a beacon might have just been removed a split second ago.

			var found = false
			for (nb, _) in LocationManager.nativeBeacons {
				if nb.major == nativeBeacon.major && nb.minor == nativeBeacon.minor {
					found = true
					break
				}
			}
			if !found {
				if Constants.logBeaconManager {
					NSLog("Skipping beacon " +
						"(\(nativeBeacon.major.intValue),\(nativeBeacon.minor.intValue)) because " +
						"it's only present in the half second old data.")
				}
				continue
			}

			if let validatedBeacon = validatedBeaconsByServiceID[serviceID] {
				validatedBeacon.update(nativeBeacon: nativeBeacon, peripheral: peripheral,
					regionIDs: regionIDs)
			}
			else if let peripheral = peripheral {
				let beacon = FTMBeacon(nativeBeacon: nativeBeacon, peripheral: peripheral,
					regionIDs: regionIDs)
				let macAddress = beacon.macAddress

				if macAddressesNotProvisioned.contains(macAddress) {
					if Constants.logBeaconManager {
						NSLog("Beacon \(macAddress) is not provisioned to anyone.")
					}
					continue
				}

				// There is no validated beacon matching the peripheral's service ID, but since
				// the service ID is derived from the major/minor, and the major/minor changes
				// from time to time as part of the Footmarks protocol, it's possible that we do
				// already have a validated beacon. In order to find it, search by MAC address.

				var found = false
				for vb in validatedBeaconsByServiceID.values where vb.macAddress == macAddress {
					found = true
					if Constants.logBeaconManager {
						NSLog("Beacon \(macAddress) has a new service ID.")
					}
					validatedBeaconsByServiceID[vb.peripheralServiceID] = nil
					vb.update(nativeBeacon: nativeBeacon, peripheral: peripheral,
						regionIDs: regionIDs)
					validatedBeaconsByServiceID[vb.peripheralServiceID] = vb
					break
				}

				if found {
					continue
				}

				if (validateRequests.filter { $0.beacon?.macAddress == macAddress }).isEmpty {
					validateRequests.insert(ValidateRequest(delegate: self,
						beacon: beacon, regionIDs: regionIDs))
				}
			}
			else {
				let openID = FTMBeacon.openID(nativeBeacon: nativeBeacon)

				if openIDsNotProvisioned.contains(openID) {
					if Constants.logBeaconManager {
						NSLog("Beacon \(openID) is not provisioned to anyone.")
					}
					continue
				}

				if (validateRequests.filter { $0.openID == openID }).isEmpty {
					let request = ValidateRequest(delegate: self, nativeBeacon: nativeBeacon,
						openID: openID, regionIDs: regionIDs)
					validateRequests.insert(request)
				}
			}
		}
	}

	class func go() {
		// Assigning the delegate creates the shared instance, if needed.
		_ = shared
	}

	@objc
	private func onAppWillEnterForeground() {
		dateWhenAppEnteredForeground = Date()
	}

	@objc
	private func onCentralManagerDidUpdatePeripherals() {
		DispatchQueue.main.async {
			self.evaluateBeaconsAndPeripherals()
		}
	}

	@objc
	private func onCentralManagerDidUpdateState() {
		DispatchQueue.main.async {
			FTMBeaconManager.delegate?.beaconManagerDidUpdateCentralManagerState?()
		}
	}

	@objc
	private func onLocationManagerDidChangeAuthorizationStatus() {
		assert(Thread.isMainThread)
		FTMBeaconManager.delegate?.beaconManagerDidChangeLocationManagerAuthorizationStatus?()
	}

	@objc
	private func onLocationManagerDidEnterCircularRegion(_ notification: Notification) {
		let region = notification.userInfo!["circularRegion"] as! CLCircularRegion
		let request = PulseCircularRegionRequest(delegate: self, region: region, regionState: .enter)
		pulseCircularRegionRequests.insert(request)
	}

	@objc
	private func onLocationManagerDidExitBeaconRegion(_ notification: Notification) {
		let region = notification.userInfo!["beaconRegion"] as! CLBeaconRegion

		// Flag all in-memory beacons in this region as expired.

		for beacon in validatedBeaconsByServiceID.values {
			if beacon.regionIDs.contains(region.identifier) {
				if Constants.logBeaconManager {
					NSLog("Expiring beacon \(beacon.macAddress) due to region exit.")
				}
				beacon.expireByForce()
				LocationManager.removeNativeBeacon(major: beacon.major, minor: beacon.minor)
			}
		}

		// Clear our knowledge of unprovisioned beacons, so that if this beacon (or any other
		// beacon) is encountered again and its ownership has changed, we will notice.

		macAddressesNotProvisioned.removeAll()
		openIDsNotProvisioned.removeAll()

		// Run the main processing routine once so that it will immediately handle expired beacons.

		process(loop: false)

		// It's possible that this region exit event launched the app from scratch, in which case
		// validatedBeaconsByServiceID wouldn't have information about which beacons are affected.
		// For this reason, evaluate the persisted beacons to see if any belong to this region,
		// and if so, perform a pulse exit on those beacons.

		var beaconInfos = Settings.beaconInfos

		if Constants.logBeaconManager {
			if beaconInfos.isEmpty {
				NSLog("During an exit region, there were NO persisted beacons.")
			}
			else {
				let array = beaconInfos.map { "\nid=\($0.key) regions=\($0.value.regionIDs)" }
				let data = array.sorted().joined(separator: ",")
				NSLog("During an exit region, these were the persisted beacons: \(data)")
			}
		}

		for (beaconID, val) in beaconInfos where val.regionIDs.contains(region.identifier) {
			if Constants.logBeaconManager {
				NSLog("Doing a pulse exit due to region exit. Persisted beacon ID = \(beaconID)")
			}
			let request = PulseBeaconRequest(delegate: self, beaconIDForRegionExit: beaconID)
			pulseBeaconRequests.insert(request)
			beaconInfos[beaconID] = nil
		}

		Settings.beaconInfos = beaconInfos

		// This region may or may not represent a hyper region, but either way, call
		// removeHyperRegion in case it is. When exiting a hyper region, we no longer want to be
		// registered for that region since it has served its purpose of generating an exit event
		// specific to that beacon.

		Regions.removeHyperRegion(region)
	}

	@objc
	private func onLocationManagerDidExitCircularRegion(_ notification: Notification) {
		let region = notification.userInfo!["circularRegion"] as! CLCircularRegion
		let request = PulseCircularRegionRequest(delegate: self, region: region, regionState: .exit)
		pulseCircularRegionRequests.insert(request)
	}

	@objc
	private func onLocationManagerDidFail() {
		assert(Thread.isMainThread)
		guard let error = LocationManager.error else {
			assertionFailure("The location manager has no error!")
			return
		}
		FTMBeaconManager.delegate?.beaconManagerDidFail(error: error)
	}

	@objc
	private func onLocationManagerDidUpdateBeacons() {
		assert(Thread.isMainThread)
		CentralManager.updateNativeBeacons(Set(LocationManager.nativeBeacons.map { $0.beacon }))
		evaluateBeaconsAndPeripherals()
	}

	@objc
	private func onLocationManagerDidUpdateSignificantLocation() {
		guard let location = LocationManager.recentLocation else {
			assertionFailure("The location is nil!")
			return
		}
		pulseLocationRequests.insert(PulseLocationRequest(delegate: self, location: location))
	}

	fileprivate func process(loop: Bool) {
		var beaconInfos = Settings.beaconInfos

		let inForegroundForAWhile = UIApplication.shared.applicationState != .background &&
			dateWhenAppEnteredForeground.timeIntervalSinceNow <= -30.0

		if Constants.logBeaconManager {
			NSLog("In foreground for a while = \(inForegroundForAWhile).")
		}

		// Remove expired beacons and kick off pulse requests. Note that naturally expired beacons
		// are removed only if the app has been in the foreground for a while. This is to deal with
		// the fact that when the app is in the background, ranging might not occur, yet beacons
		// could still be in range.

		for beacon in validatedBeaconsByServiceID.values {
			if beacon.isExpiredByForce || (beacon.isExpiredNaturally && inForegroundForAWhile) {
				// Do a pulse exit and remove the beacon.
				if Constants.logBeaconManager {
					NSLog("Doing a pulse exit for beacon \(beacon.macAddress)")
				}
				beacon.regionState = .exit
				pulseBeaconRequests.insert(PulseBeaconRequest(delegate: self, beacon: beacon))
				beaconInfos[beacon.id] = nil
				validatedBeaconsByServiceID[beacon.peripheralServiceID] = nil
			}
			else if beacon.dateOfNextPulse.timeIntervalSinceNow <= 0.0 {
				// It's time to do a pulse.
				if (pulseBeaconRequests.filter { $0.beaconID == beacon.id }).isEmpty {
					pulseBeaconRequests.insert(PulseBeaconRequest(delegate: self, beacon: beacon))
				}
			}
		}

		// If the app has been in the foreground for a while, check to see if any persisted
		// beacon IDs have expired and kick off pulse exits as needed.

		if inForegroundForAWhile {
			for (beaconID, val) in beaconInfos {
				if FTMBeacon.isExpired(dateOfLastImpression: val.dateOfLastImpression) {
					if Constants.logBeaconManager {
						NSLog("Doing a pulse exit. Persisted beacon ID = \(beaconID)")
					}
					let request = PulseBeaconRequest(delegate: self, beaconIDForRegionExit: beaconID)
					pulseBeaconRequests.insert(request)
					beaconInfos[beaconID] = nil
				}
			}
		}

		// Add to (but don't remove from) the persisted beacons.

		for beacon in validatedBeaconsByServiceID.values {
			if var info = beaconInfos[beacon.id] {
				info.dateOfLastImpression = Date()
				info.regionIDs = beacon.regionIDs
				beaconInfos[beacon.id] = info
			}
			else {
				beaconInfos[beacon.id] = (Date(), beacon.regionIDs)
			}
		}

		// Persist the beacons and loop.

		Settings.beaconInfos = beaconInfos

		if loop {
			if Constants.logBeaconManager {
				let macs = (FTMBeaconManager.beacons.map { $0.macAddress }).sorted()
				NSLog("Beacons = [\(macs.joined(separator: ", "))]")
			}
			FTMBeaconManager.delegate?.beaconManagerDidRangeBeacons()
			DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
				self.process(loop: true)
			}
		}
	}

}

// Implement an extension for the sole purpose of avoiding references to pulse delegates and
// ValidateRequestDelegate in the SDK documentation.

extension FTMBeaconManager: PulseCircularRegionRequestDelegate,
	PulseBeaconRequestDelegate,
	PulseLocationRequestDelegate,
	ValidateRequestDelegate
{
	func pulseBeaconRequestDidFinish(_ request: PulseBeaconRequest, error: Error?) {
		if let error = error {
			if Constants.logBeaconManager {
				NSLog("The pulse beacon requested failed. \(error)")
			}
		}
		if !request.experiences.isEmpty {
			FTMExperienceManager.delegate?.experienceManagerDidReceiveExperiences(
				request.experiences)
		}
		pulseBeaconRequests.remove(request)
	}

	func pulseCircularRegionRequestDidFinish(_ request: PulseCircularRegionRequest, error: Error?) {
		if let error = error {
			if Constants.logBeaconManager {
				NSLog("The circular region pulse requested failed. \(error)")
			}
		}
		if !request.experiences.isEmpty {
			FTMExperienceManager.delegate?.experienceManagerDidReceiveExperiences(
				request.experiences)
		}
		pulseCircularRegionRequests.remove(request)
	}

	func pulseLocationRequestDidFinish(_ request: PulseLocationRequest, error: Error?) {
		if let error = error {
			if Constants.logBeaconManager {
				NSLog("The pulse location requested failed. \(error)")
			}
		}
		if !request.experiences.isEmpty {
			FTMExperienceManager.delegate?.experienceManagerDidReceiveExperiences(
				request.experiences)
		}
		pulseLocationRequests.remove(request)
	}

	func validateRequestDidFinish(_ request: ValidateRequest, error: Error?) {
		if let error = error {
			if Constants.logBeaconManager {
				NSLog("The validate requested failed. \(error)")
			}
		}
		else if let response = request.response {
			var theBeacon: FTMBeacon?

			if let beacon = request.beacon {
				beacon.update(
					attributes: response.attributes,
					id: response.id,
					isUnauthorized: response.isUnauthorized,
					name: response.name,
					rssiConstant: response.rssiConstant)

				// Now that the beacon is known to be valid, add it to the master list.

				theBeacon = beacon
				validatedBeaconsByServiceID[beacon.peripheralServiceID] = beacon

				if response.registerHyperRegion {
					Regions.addHyperRegion(proximityUUID: beacon.proximityUUID,
						major: beacon.major, minor: beacon.minor)
				}
			}
			else if let nativeBeacon = request.nativeBeacon {
				let beacon = FTMBeacon(
					nativeBeacon: nativeBeacon,
					attributes: response.attributes,
					id: response.id,
					isUnauthorized: response.isUnauthorized,
					macAddress: response.macAddress,
					name: response.name,
					regionIDs: request.regionIDs,
					rssiConstant: response.rssiConstant)

				theBeacon = beacon
				validatedBeaconsByServiceID[beacon.peripheralServiceID] = beacon

				if response.registerHyperRegion {
					Regions.addHyperRegion(proximityUUID: nativeBeacon.proximityUUID,
						major: nativeBeacon.major.intValue, minor: nativeBeacon.minor.intValue)
				}
			}

			if let beacon = theBeacon {
				for (key, val) in beacon.attributes {
					// Keep track of this info for nearest beacon acknowledgments.
					attributeKeyValToBeaconID[key + "|" + val] = beacon.id
				}
			}

			// Run the main processing routine once, rather than wait up to one second for the
			// loop to run naturally. This ensures that the pulse starts right away.
			process(loop: false)
		}
		else if let beacon = request.beacon {
			macAddressesNotProvisioned.insert(beacon.macAddress)
		}
		else if let openID = request.openID {
			openIDsNotProvisioned.insert(openID)
		}

		validateRequests.remove(request)
	}

}

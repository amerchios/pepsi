//
//  FTMNearestBeaconAcknowledgment.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/25/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/// Allows a nearest beacon acknowledgment to be sent to the SmartConnect Cloud.
public class FTMNearestBeaconAcknowledgment: NSObject {

	/**
		Sends an acknowledgment to the SmartConnect Cloud, providing the attribute key and value
		of the nearest beacon as perceived by the user.

		In some situations, requesting the nearest beacon may be part of a workflow intended to
		help the user easily find the nearest beacon. For example, the user might be near three
		kiosks, each of which is equipped with a beacon, and the app wants to identify the nearest
		one, such as to streamline the checkout process.

		In most cases, the SmartConnect Cloud will be able to identify the nearest beacon, but
		occasionally the user might find the result to be incorrect. The app might provide a user
		interface that lets the user manually select a different kiosk. In order to train the
		cloud, it is helpful if the app can send an acknowledgment with attributes that specify
		which kiosk was in fact the nearest, allowing the cloud to improve results over time.

		In these cases, the app should send an acknowledgment with the correct attributes,
		regardless of the result being correct or not. In situations where the app cannot provide
		meaningful training data to the cloud, an acknowledgment should not be sent.

		- parameters:
			- resultID: The value of `FTMNearestBeaconResult.id`
			- attributeKey: The key name in the attributes of the beacon that is nearest according
				to the user.
			- attributeValue: The value of the key in the attributes of the beacon that is nearest
				according to the user.
			- completion: The completion handler.

		- seealso:
			- `FTMBeacon.attributes`
			- `FTMNearestBeaconResult`
	*/
	@discardableResult
	@objc
	public class func send(
		resultID: String,
		attributeKey: String,
		attributeValue: String,
		completion: ((Error?) -> Void)?) -> FTMRequestHandle
	{
		guard !resultID.isEmpty, !attributeKey.isEmpty, !attributeValue.isEmpty else {
			DispatchQueue.main.async {
				completion?(FTMError.parameter.error)
			}
			return FTMRequestHandle()
		}

		if resultID.hasPrefix(Constants.nearestBeaconClientSideResultIDPrefix) {
			if let correctBeaconID = FTMBeaconManager.beaconIDMatching(
				attributeKey: attributeKey, attributeValue: attributeValue)
			{
				for result in FTMNearestBeaconResult.recentResults where result.id == resultID {
					let request = SendNearestBeaconAck(beacons: FTMBeaconManager.beacons,
						correctBeaconID: correctBeaconID, result: result, completion: completion)
					return request.handle!
				}
			}

			DispatchQueue.main.async {
				completion?(nil)
			}
			return FTMRequestHandle()
		}

		let request = SendConversion(
			action: attributeKey,
			customActionName: "",
			customValueName: "",
			eventID: resultID,
			value: attributeValue,
			valueType: nil,
			completion: completion)

		return request.handle!
	}

}

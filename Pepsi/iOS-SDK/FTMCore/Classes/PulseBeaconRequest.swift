//
//  PulseBeaconRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/16/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

protocol PulseBeaconRequestDelegate: class {
	func pulseBeaconRequestDidFinish(_ request: PulseBeaconRequest, error: Error?)
}

class PulseBeaconRequest: Hashable {

	let beaconID: String
	private weak var delegate: PulseBeaconRequestDelegate?
	private(set) var experiences = Set<FTMExperience>()
	private var impl: PulseBeaconRequestImpl?

	var hashValue: Int {
		return ObjectIdentifier(self).hashValue
	}

	init(delegate: PulseBeaconRequestDelegate, beacon: FTMBeacon) {
		self.beaconID = beacon.id
		self.delegate = delegate

		if beacon.regionState == .idle && (beacon.isExpiredNaturally || beacon.isExpiredByForce) {
			weak var wself = self
			DispatchQueue.main.async {
				guard let sself = wself else { return }
				if Constants.logBeaconManager {
					NSLog("Skipping the pulse idle request because the beacon has expired.")
				}
				beacon.dateOfNextPulse = Date(timeIntervalSinceNow: 30)
				sself.delegate?.pulseBeaconRequestDidFinish(sself, error: nil)
			}
		}
		else {
			impl = PulseBeaconRequestImpl(parent: self, beacon: beacon)
		}
	}

	init(delegate: PulseBeaconRequestDelegate, beaconIDForRegionExit beaconID: String) {
		self.beaconID = beaconID
		self.delegate = delegate
		impl = PulseBeaconRequestImpl(parent: self, beaconIDForRegionExit: beaconID)
	}

	static func ==(lhs: PulseBeaconRequest, rhs: PulseBeaconRequest) -> Bool {
		return lhs === rhs
	}

	fileprivate func implDidFinish(experiences: Set<FTMExperience>, error: Error?) {
		self.experiences = experiences
		delegate?.pulseBeaconRequestDidFinish(self, error: error)
	}

	class func parseExperiencesFromServerDictionary(_ dictionary: [String: Any]) ->
		Set<FTMExperience>
	{
		var experiences = Set<FTMExperience>()

		if let eventID = dictionary["eventId"] as? String,
			!eventID.isEmpty,
			let payloads = dictionary["payloads"] as? [[String: Any]]
		{
			for payload in payloads {
				guard let payloadType = payload["type"] as? String,
					let experienceType = FTMExperienceType(payloadType: payloadType),
					let payloadAction = payload["action"] as? String,
					let engagement = FTMExperienceEngagement(payloadAction: payloadAction)
				else {
					continue
				}

				let contentDict = payload["content"] as? [String: Any] ?? [:]
				let content: String

				switch experienceType {
					case .alert:
						content = ""
					case .custom, .html:
						content = contentDict["text"] as? String ?? ""
					case .image, .url, .video:
						content = contentDict["url"] as? String ?? ""
				}

				experiences.insert(FTMExperience(
					content: content,
					engagement: engagement,
					eventID: eventID,
					experienceType: experienceType,
					name: payload["name"] as? String ?? "",
					promptDescription: payload["notifDescription"] as? String ?? "",
					promptTitle: payload["notifTitle"] as? String ?? "")
				)
			}
		}

		return experiences
	}

}

private class PulseBeaconRequestImpl: WebServiceRequest {

	private var beacon: FTMBeacon?
	private weak var parent: PulseBeaconRequest?

	init(parent: PulseBeaconRequest, beacon: FTMBeacon) {
		self.beacon = beacon
		self.parent = parent

		var body: [String: Any] = [
			"beaconId": beacon.id,
			"distance": beacon.accuracy,
			"mac": beacon.macAddress,
			"proximityUUID": beacon.proximityUUID.uuidString,
			"regionState": beacon.regionState.string,
			"rssi": beacon.rssi,
		]

		if let batteryLevel = beacon.batteryLevel {
			body["battery"] = Int(round(batteryLevel * 100.0))
		}

		body = WebServiceRequest.addLocation(body: body)
		super.init(path: "pulse", body: body)
	}

	init(parent: PulseBeaconRequest, beaconIDForRegionExit beaconID: String) {
		self.parent = parent

		var body: [String: Any] = [
			"beaconId": beaconID,
			"regionState": FTMBeacon.RegionState.exit.string,
		]

		body = WebServiceRequest.addLocation(body: body)
		super.init(path: "pulse", body: body)
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		if error == nil {
			if let beacon = beacon, beacon.regionState == .enter {
				beacon.regionState = .idle
			}
		}
		let experiences = PulseBeaconRequest.parseExperiencesFromServerDictionary(dict)
		let delay = dict["pulsedelay"] as? TimeInterval ?? 30.0
		beacon?.dateOfNextPulse = Date(timeIntervalSinceNow: delay)
		parent?.implDidFinish(experiences: experiences, error: error)
	}

}

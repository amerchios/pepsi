//
//  FTMEnum.swift
//  FTMCore
//
//  Created by Shane Meyer on 6/1/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/// The action taken for the experience conversion.
@objc
public enum FTMExperienceAction: Int {

	/// The conversion happened automatically.
	case automated

	/// The content was tapped or clicked.
	case clicked

	/// The content was listened to.
	case listened

	/// No action was taken.
	case none

	/// The content was opened.
	case opened

	/// The content was retargeted.
	case retargeted

	/// The content was shared.
	case shared

	/// The content was swiped.
	case swiped

	/// The content was watched.
	case watched

	var string: String {
		switch self {
			case .automated:
				return "automated"
			case .clicked:
				return "click"
			case .listened:
				return "listened"
			case .none:
				return "none"
			case .opened:
				return "opened"
			case .retargeted:
				return "retargeted"
			case .shared:
				return "shared"
			case .swiped:
				return "swiped"
			case .watched:
				return "watched"
		}
	}

}

/// The engatement types for experiences.
@objc
public enum FTMExperienceEngagement: Int {

	/// When the experience is delivered, it is meant to be presented immediately.
	case autoShow

	/// When the experience is delivered, no engagement with the user is expected.
	case passive

	/// When the experience is delivered, a prompt is meant to be displayed before presenting
	/// the experience.
	case prompt

	init?(payloadAction: String) {	
		switch payloadAction {
			case "autoShow":
				self = .autoShow
			case "passive":
				self = .passive
			case "prompt":
				self = .prompt
			default:
				return nil
		}
	}

}

/// The experience types.
///
/// The custom type is often a good choice since it has no predefined meaning and can handle
/// any situation.
@objc
public enum FTMExperienceType: Int {

	/// An alert, often paired with `FTMExperienceEngagement.prompt` for displaying a title and
	/// description in something like a `UIAlertController` or a notification.
	case alert

	/// An experience with no predefined meaning.
	case custom

	/// An HTML string.
	case html

	/// An image URL.
	case image

	/// A URL.
	case url

	/// A video URL.
	case video

	init?(payloadType: String) {
		switch payloadType {
			case "alert":
				self = .alert
			case "api":
				self = .custom
			case "custom":
				self = .custom
			case "demo":
				self = .custom
			case "html":
				self = .html
			case "image":
				self = .image
			case "passive":
				self = .custom
			case "url":
				self = .url
			case "video":
				self = .video
			default:
				return nil
		}
	}

}

/// The type of the value used when sending the conversion.
@objc
public enum FTMExperienceValueType: Int {

	/// The value represents a currency amount.
	case currency

	/// The value represents the number of minutes.
	case minutes

	/// The value represents a quantity.
	case quantity

	/// The value represents the number of seconds.
	case seconds

	var string: String {
		switch self {
			case .currency:
				return "currency"
			case .minutes:
				return "timeInMinutes"
			case .quantity:
				return "quantity"
			case .seconds:
				return "timeInSeconds"
		}
	}

}

//
//  SendNearestBeaconAck.swift
//  FTMCore
//
//  Created by Shane Meyer on 2/14/18.
//  Copyright © 2018 Footmarks, Inc. All rights reserved.
//

import Foundation

class SendNearestBeaconAck: WebServiceRequest {

	private let completion: ((Error?) -> Void)?

	init(beacons: Set<FTMBeacon>,
		correctBeaconID: String,
		result: FTMNearestBeaconResult,
		completion: ((Error?) -> Void)?)
	{
		self.completion = completion

		let body: [String: Any] = [
			"confidenceLevel": round(result.confidence * 100.0),
			"correctBeacon": correctBeaconID,
			"inRangeBeacons": beacons.map {[
				"accuracy": $0.accuracyCached,
				"beacon": $0.id,
				"rssi": [$0.rssiCached],
			]},
			"nearestBeacon": result.beacon.id,
		]

		super.init(path: "beacon/nearestack", body: body)
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		completion?(error)
	}

}

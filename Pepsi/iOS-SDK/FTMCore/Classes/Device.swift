//
//  Device.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/3/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import AdSupport

class Device {

	static var allowIDFA = true

	class var id: (id: String, isIDFA: Bool) {
		let s = allowIDFA ? ASIdentifierManager.shared().advertisingIdentifier.uuidString : ""
		if s.isEmpty || s == "00000000-0000-0000-0000-000000000000" {
			return (UIDevice.current.identifierForVendor?.uuidString ?? "", false)
		}
		return (s, true)
	}

}

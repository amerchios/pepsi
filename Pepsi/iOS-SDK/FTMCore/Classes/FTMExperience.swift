//
//  FTMExperience.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/23/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

private let keyContent = "content"
private let keyEngagement = "engagement"
private let keyEventID = "eventID"
private let keyName = "name"
private let keyPromptDescription = "promptDescription"
private let keyPromptTitle = "promptTitle"
private let keyType = "type"

/// An experience configured in the SmartConnect Cloud.
public class FTMExperience: NSObject, NSCoding {

	private let eventID: String

	/**
		The content of the experience.

		The nature of the content depends on the experience type.

		- alert: (unused)
		- custom: Any custom value
		- html: The HTML
		- image: The image URL
		- url: The URL
		- video: The video URL
	*/
	@objc
	public let content: String

	/// The type of engagement to have with the user.
	@objc
	public let engagement: FTMExperienceEngagement

	/// The type of experience.
	@objc
	public let experienceType: FTMExperienceType

	/// The hash value.
	@objc
	public override var hashValue: Int {
		return eventID.hashValue
	}

	/// The name of the payload as configured on the management console.
	@objc
	public let name: String

	/// The description when `engagement` is of type `prompt`.
	@objc
	public let promptDescription: String

	/// The title when `engagement` is of type `prompt`.
	@objc
	public let promptTitle: String

	init(content: String,
		engagement: FTMExperienceEngagement,
		eventID: String,
		experienceType: FTMExperienceType,
		name: String,
		promptDescription: String,
		promptTitle: String)
	{
		self.content = content
		self.engagement = engagement
		self.eventID = eventID
		self.experienceType = experienceType
		self.name = name
		self.promptDescription = promptDescription
		self.promptTitle = promptTitle
	}

	/**
		Creates an experience using the given unarchiver.

		- parameters:
			- decoder: An unarchiver object.
	*/
	@objc
	public required convenience init?(coder decoder: NSCoder) {
		let engagementInt = decoder.decodeInteger(forKey: keyEngagement)
		let experienceTypeInt = decoder.decodeInteger(forKey: keyType)

		guard
			let content = decoder.decodeObject(forKey: keyContent) as? String,
			let engagement = FTMExperienceEngagement(rawValue: engagementInt),
			let eventID = decoder.decodeObject(forKey: keyEventID) as? String,
			let experienceType = FTMExperienceType(rawValue: experienceTypeInt),
			let name = decoder.decodeObject(forKey: keyName) as? String,
			let promptDescription = decoder.decodeObject(forKey: keyPromptDescription) as? String,
			let promptTitle = decoder.decodeObject(forKey: keyPromptTitle) as? String
		else {
			return nil
		}
		self.init(
			content: content,
			engagement: engagement,
			eventID: eventID,
			experienceType: experienceType,
			name: name,
			promptDescription: promptDescription,
			promptTitle: promptTitle)
	}

	/// Evaluates two experiences for equality.
	public static func ==(lhs: FTMExperience, rhs: FTMExperience) -> Bool {
		return
			lhs.content == rhs.content &&
			lhs.engagement == rhs.engagement &&
			lhs.eventID == rhs.eventID &&
			lhs.experienceType == rhs.experienceType &&
			lhs.name == rhs.name &&
			lhs.promptDescription == rhs.promptDescription &&
			lhs.promptTitle == rhs.promptTitle
	}

	/**
		Encodes the experience using the given archiver.

		- parameters:
			- encoder: An archiver object.
	*/
	@objc
	public func encode(with encoder: NSCoder) {
		encoder.encode(content, forKey: keyContent)
		encoder.encode(engagement.rawValue, forKey: keyEngagement)
		encoder.encode(eventID, forKey: keyEventID)
		encoder.encode(experienceType.rawValue, forKey: keyType)
		encoder.encode(name, forKey: keyName)
		encoder.encode(promptDescription, forKey: keyPromptDescription)
		encoder.encode(promptTitle, forKey: keyPromptTitle)
	}

	/**
		Sends a conversion to the SmartConnect Cloud to acknowledge that the experience has been
		received and processed.

		- parameters:
			- action: The action that was taken.
			- valueType: The type of the value.
			- value: The value associated with the conversion.
			- completion: The completion handler.
		- returns:
			A request handle.
	*/
	@discardableResult
	@objc
	public func sendConversion(
		action: FTMExperienceAction,
		valueType: FTMExperienceValueType,
		value: Double,
		completion: ((Error?) -> Void)?) -> FTMRequestHandle
	{
		guard !eventID.isEmpty else {
			DispatchQueue.main.async {
				completion?(FTMError.parameter.error)
			}
			return FTMRequestHandle()
		}

		let request = SendConversion(
			action: action.string,
			customActionName: "",
			customValueName: "",
			eventID: eventID,
			value: "\(value)",
			valueType: valueType,
			completion: completion)

		return request.handle!
	}

	/**
		Sends a conversion to the SmartConnect Cloud to acknowledge that the experience has been
		received and processed.

		- parameters:
			- customActionName: The name of the custom action that was taken.
			- customValueName: The name of the custom value.
			- value: The value associated with the conversion.
			- completion: The completion handler.
		- returns:
			A request handle.
	*/
	@discardableResult
	@objc
	public func sendConversion(
		customActionName: String,
		customValueName: String,
		value: Double,
		completion: ((Error?) -> Void)?) -> FTMRequestHandle
	{
		guard !eventID.isEmpty, !customActionName.isEmpty, !customValueName.isEmpty else {
			DispatchQueue.main.async {
				completion?(FTMError.parameter.error)
			}
			return FTMRequestHandle()
		}

		let request = SendConversion(
			action: nil,
			customActionName: customActionName,
			customValueName: customValueName,
			eventID: eventID,
			value: "\(value)",
			valueType: nil,
			completion: completion)

		return request.handle!
	}

}

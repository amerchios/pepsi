//
//  FTMRequestHandle.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/3/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/// A handle returned by a network request, allowing the request to be canceled.
///
/// Network requests follow a completion handler pattern that makes it easy to take action once
/// a request has finished. When creating a request, a handle is returned to the caller.
///
/// It is not necessary to use or make a strong reference to a handle. The network request will
/// continue until it has completed. To cancel a request, call `cancel()` on its handle.
public class FTMRequestHandle: NSObject {

	private static var handles = [FTMRequestHandle]()
	private static let lock = NSObject()
	private var request: WebServiceRequest?

	override init() {
		// This allows a handle to represent no request at all, which is handy with some APIs.
	}

	init(request: WebServiceRequest) {
		self.request = request
		super.init()
		objc_sync_enter(FTMRequestHandle.lock)
		FTMRequestHandle.handles.append(self)
		objc_sync_exit(FTMRequestHandle.lock)
	}

	deinit {
		cancel()
	}

	/// Cancels the network request associated with the handle.
	///
	/// It is harmless to cancel the same request more than once.
	@objc
	public func cancel() {
		objc_sync_enter(FTMRequestHandle.lock)
		for (i, handle) in FTMRequestHandle.handles.enumerated() where handle === self {
			FTMRequestHandle.handles.remove(at: i)
		}
		objc_sync_exit(FTMRequestHandle.lock)
		request = nil
	}

}

//
//  FTMNearestBeaconRequest.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/4/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

/// A request that returns the beacon nearest to the current device.
public class FTMNearestBeaconRequest: NSObject {

	/**
		Starts a request that returns the beacon nearest to the current device.

		If no beacons are present, the result will be `nil` and there will be no error.

		- parameters:
			- completion: The completion handler.
		- returns:
			A request handle.
	*/
	@discardableResult
	@objc
	public class func begin(
		completion: @escaping (FTMNearestBeaconResult?, Error?) -> Void) -> FTMRequestHandle
	{
		return begin(attributeFilterKey: "", attributeFilterValue: "", completion: completion)
	}

	/**
		Starts a request that returns the beacon nearest to the current device, and that matches
		the given attribute key and value.

		A beacon will be returned by the completion handler only if it matches the given attribute
		key and value. A beacon's attributes are configured on the management console, and are
		accessible via `FTMBeacon.attributes`.

		If no beacons are present, the result will be `nil` and there will be no error.

		- parameters:
			- attributeFilterKey: A key in a beacon's attributes dictionary to match on.
			- attributeFilterValue: A value in a beacon's attributes dictionary to match on.
			- completion: The completion handler.
		- returns:
			A request handle.
		- seealso:
			`FTMBeacon.attributes`
	*/
	@discardableResult
	@objc
	public class func begin(
		attributeFilterKey: String,
		attributeFilterValue: String,
		completion: @escaping (FTMNearestBeaconResult?, Error?) -> Void) -> FTMRequestHandle
	{
		let beacons = FTMBeaconManager.beacons

		if beacons.isEmpty {
			DispatchQueue.main.async {
				completion(nil, nil)
			}
			return FTMRequestHandle()
		}

		if Settings.nearestBeaconModeIsCloud {
			let request = FTMNearestBeaconRequestCloudImpl(
				attributeFilterKey: attributeFilterKey,
				attributeFilterValue: attributeFilterValue,
				beacons: beacons,
				completion: completion)
			return request.handle!
		}

		// Rather than make a cloud request, determine client-side which beacon is nearest.

		DispatchQueue.main.async {
			var bestBeacon: FTMBeacon!
			let hasFilter = !attributeFilterKey.isEmpty || !attributeFilterValue.isEmpty
			var viableCount = 0

			for beacon in beacons {
				var isViable = false

				if hasFilter {
					for (key, val) in beacon.attributes where
						key.lowercased() == attributeFilterKey.lowercased() &&
						val.lowercased() == attributeFilterValue.lowercased()
					{
						isViable = true
						break
					}
				}
				else {
					isViable = true
				}

				guard isViable else { continue }
				viableCount += 1

				if bestBeacon == nil || bestBeacon.accuracyCachedUsingFootmarksAlgorithm < 0 {
					bestBeacon = beacon
				}
				else {
					let a = beacon.accuracyCachedUsingFootmarksAlgorithm
					if a >= 0 && a < bestBeacon.accuracyCachedUsingFootmarksAlgorithm {
						bestBeacon = beacon
					}
				}
			}

			if bestBeacon == nil {
				completion(nil, nil)
			}
			else {

				// Calculate the confidence according to these rules:
				// 1. Only 1 beacon in range that meets the filter = 100%
				// 2. More than one beacon in range that meets the filter, or no filter = 80%

				let conf: Double
				if hasFilter {
					conf = (viableCount == 1) ? 1.0 : 0.8
				}
				else {
					conf = 0.8
				}

				let id = Constants.nearestBeaconClientSideResultIDPrefix +
					UUID().uuidString.replacingOccurrences(of: "-", with: "")
				let result = FTMNearestBeaconResult(beacon: bestBeacon, confidence: conf, id: id)
				completion(result, nil)
			}
		}

		return FTMRequestHandle()
	}

}

private class FTMNearestBeaconRequestCloudImpl: WebServiceRequest {

	private let beacons: Set<FTMBeacon>
	private let completion: (FTMNearestBeaconResult?, Error?) -> Void

	init(attributeFilterKey: String,
		attributeFilterValue: String,
		beacons: Set<FTMBeacon>,
		completion: @escaping (FTMNearestBeaconResult?, Error?) -> Void)
	{
		self.beacons = beacons
		self.completion = completion

		var body: [String: Any] = [
			"inRangeBeacons": beacons.map {[
				"beacon": $0.id,
				"osAccuracy": $0.accuracyCached,
				"rssi": [$0.rssiCached],
			]}
		]

		if !attributeFilterKey.isEmpty {
			body["attributeFilter"] = [
				"key": attributeFilterKey,
				"value": attributeFilterValue,
			]
		}

		super.init(path: "beacon/getnearest", body: body)
	}

	override func requestDidFinishWithResponseDict(_ dict: [String : Any], error: Error?) {
		if let beaconID = dict["nearestBeacon"] as? String,
			!beaconID.isEmpty,
			let confidence = dict["confidenceLevel"] as? Double,
			let eventID = dict["event"] as? String,
			!eventID.isEmpty
		{
			for beacon in beacons where beacon.id == beaconID {
				let result = FTMNearestBeaconResult(
					beacon: beacon,
					confidence: min(1.0, max(0.0, confidence / 100.0)),
					id: eventID)
				completion(result, nil)
				return
			}
		}
		completion(nil, error)
	}

}

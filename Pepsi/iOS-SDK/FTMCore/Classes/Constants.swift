//
//  Constants.swift
//  FTMCore
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import Foundation

class Constants {

	private static let bundle = Bundle(for: Constants.self)
	private static let isProduction = true
	static let logBeaconManager = false
	static let logCentralManager = false
	static let logLocationManager = false
	static let logNetwork = false
	static let nearestBeaconClientSideResultIDPrefix = "CL:"
	static let regionIDPrefix = "ftmr"

	static let frameworkBuild: String = {
		let val = bundle.infoDictionary?["CFBundleVersion"]
		return val as? String ?? ""
	}()

	static let frameworkVersion: String = {
		let val = bundle.infoDictionary?["CFBundleShortVersionString"]
		return val as? String ?? ""
	}()

	static let hasBackgroundModeBluetoothCentral: Bool = {
		if let array = Bundle.main.infoDictionary?["UIBackgroundModes"] as? [String] {
			return array.contains("bluetooth-central")
		}
		return false
	}()

	static let hasBluetoothPeripheralUsageDescription: Bool = {
		return hasInfoKey("NSBluetoothPeripheralUsageDescription")
	}()

	static let hasLocationAlwaysAndWhenInUseUsageDescription: Bool = {
		return hasInfoKey("NSLocationAlwaysAndWhenInUseUsageDescription")
	}()

	static let hasLocationAlwaysUsageDescription: Bool = {
		return hasInfoKey("NSLocationAlwaysUsageDescription")
	}()

	static let hasLocationWhenInUseUsageDescription: Bool = {
		return hasInfoKey("NSLocationWhenInUseUsageDescription")
	}()

	static let urlSignIn: URL = {
		return isProduction ?
			URL(string: "https://auth.footmarks.com/oauth/token")! :
			URL(string: "https://test.auth.footmarks.com/oauth/token")!
	}()

	static let urlVerifyAccess: URL = {
		return isProduction ?
			URL(string: "https://auth.footmarks.com/oauth/verifyaccess")! :
			URL(string: "https://test.auth.footmarks.com/oauth/verifyaccess")!
	}()

	static let urlWebService: URL = {
		return isProduction ?
			URL(string: "https://api.footmarks.com/")! :
			URL(string: "https://test.api.footmarks.com/")!
	}()

	private class func hasInfoKey(_ key: String) -> Bool {
		if let s = Bundle.main.infoDictionary?[key] as? String {
			return !s.isEmpty
		}
		return false
	}

}

//
//  AppDelegate.swift
//  SampleAppSwift
//
//  Created by Shane Meyer on 5/30/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import FTMCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions
		launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
	{
		let window = UIWindow()
		self.window = window
		window.rootViewController = ViewController()
		window.makeKeyAndVisible()

		// Insert your app key and secret below.

		FTMSession.signIn(appKey: "", appSecret: "", username: nil) { (error: Error?) in
			if let error = error {
				if ViewController.instance != nil {
					AppDelegate.showError(error)
				}
				else {
					NSLog("Sign-in error: \(error)")
				}
			}
			else {
				// Sign-in was successful. Now other FTMCore classes can be called.
				BeaconManager.go()
				ExperienceManager.go()
			}
		}

		return true
	}

	static func showError(_ error: Error) {
		showMessage(error.localizedDescription)
	}

	static func showMessage(_ message: String) {
		let vc = UIAlertController(title: "", message: message, preferredStyle: .alert)
		vc.addAction(UIAlertAction(title: "OK", style: .default))
		ViewController.instance?.present(vc, animated: true)
	}

}

//
//  BeaconManager.swift
//  SampleAppSwift
//
//  Created by Shane Meyer on 6/1/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation
import FTMCore

class BeaconManager: FTMBeaconManagerDelegate {

	static let eventDidRangeBeacons = Notification.Name("Sample.BeaconManagerDRB")
	private static let shared = BeaconManager()

	private init() {
		FTMBeaconManager.delegate = self
	}

	func beaconManagerDidChangeLocationManagerAuthorizationStatus() {
		let status = CLLocationManager.authorizationStatus()
		NSLog("The location manager authorization status is now \(status.rawValue).")
	}

	func beaconManagerDidFail(error: Error) {
		NSLog("Beacon manager error: \(error)")
	}

	func beaconManagerDidRangeBeacons() {
		NotificationCenter.default.post(name: BeaconManager.eventDidRangeBeacons, object: self)
	}

	func beaconManagerDidUpdateCentralManagerState() {
		let state = FTMBeaconManager.centralManagerState
		NSLog("The central manager state is now \(state.rawValue).")
	}

	class func go() {
		// Create the shared instance, if needed.
		_ = shared
	}

}

//
//  ViewController.swift
//  SampleAppSwift
//
//  Created by Shane Meyer on 5/30/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import FTMCore

class ViewController: UIViewController {

	private(set) static weak var instance: ViewController?
	private weak var label: UILabel?
	private var processedExperienceTypes = [String]()

	init() {
		super.init(nibName: nil, bundle: nil)
		ViewController.instance = self

		// Register for user notifications as part of the demonstration of "alert" experiences.

		let settings = UIUserNotificationSettings(types: .alert, categories: nil)
		UIApplication.shared.registerUserNotificationSettings(settings)

		// Register for beacon manager ranging and processed experiences.

		let nc = NotificationCenter.default
		nc.addObserver(self, selector: #selector(onBeaconManagerDidRangeBeacons),
			name: BeaconManager.eventDidRangeBeacons, object: nil)
		nc.addObserver(self, selector: #selector(onExperienceManagerDidProcessExperience(_:)),
			name: ExperienceManager.eventDidProcessExperience, object: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	override func loadView() {
		view = UIView()
		view.backgroundColor = .white

		let label = UILabel()
		self.label = label
		label.font = UIFont(name: "Menlo-Regular", size: 20) ?? .systemFont(ofSize: 20)
		label.numberOfLines = 0
		label.textAlignment = .left
		label.textColor = .black
		view.addSubview(label)

		updateUI()
	}

	@objc
	private func onBeaconManagerDidRangeBeacons() {
		updateUI()
	}

	@objc
	private func onExperienceManagerDidProcessExperience(_ notification: Notification) {
		processedExperienceTypes.append(notification.userInfo!["type"] as! String)
		updateUI()
	}

	private func updateUI() {
		var s = "Beacons in Range\n----------------"

		if FTMSession.isSignedIn {
			let beacons = FTMBeaconManager.beacons.sorted { $0.macAddress < $1.macAddress }
			for beacon in beacons {
				s += "\n" + beacon.macAddress
			}
		}

		s += "\n\nProcessed Experiences\n---------------------"
		for experienceType in processedExperienceTypes {
			s += "\n" + experienceType
		}

		label?.text = s
		viewIfLoaded?.setNeedsLayout()
	}

	override func viewDidLayoutSubviews() {
		guard let label = label else { return }
		let size = view.bounds.size
		label.frame = CGRect(x: 0, y: 0, width: 1024, height: 1)
		label.sizeToFit()
		label.frame = label.bounds.offsetBy(dx: round((size.width - label.bounds.width) / 2), dy: 64)
	}

}

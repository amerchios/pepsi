//
//  ExperienceManager.swift
//  SampleAppSwift
//
//  Created by Shane Meyer on 6/1/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import FTMCore

private let keyExperiences = "SampleAppExperiences"

class ExperienceManager: FTMExperienceManagerDelegate {

	static let eventDidProcessExperience = Notification.Name("Sample.ExperienceManagerDPE")
	private static let shared = ExperienceManager()

	private init() {
		FTMExperienceManager.delegate = self
		NotificationCenter.default.addObserver(self, selector: #selector(onAppWillEnterForeground),
			name: .UIApplicationWillEnterForeground, object: nil)
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	func experienceManagerDidReceiveExperiences(_ experiences: Set<FTMExperience>) {
		let inBackground = (UIApplication.shared.applicationState == .background)

		// Process experiences with prompts.

		for exp in experiences where !exp.promptTitle.isEmpty || !exp.promptDescription.isEmpty {
			if inBackground {
				let n = UILocalNotification()
				n.alertTitle = exp.promptTitle
				n.alertBody = exp.promptDescription
				UIApplication.shared.presentLocalNotificationNow(n)
			}
			else {
				let vc = UIAlertController(title: exp.promptTitle, message: exp.promptDescription,
					preferredStyle: .alert)
				vc.addAction(UIAlertAction(title: "OK", style: .default))
				ViewController.instance?.present(vc, animated: true)
			}

			if exp.experienceType == .alert {

				// Alert experiences have no further information to share. Send a conversion to
				// inform the SmartConnect Cloud. Specify an action and value appropriate for
				// your app.

				exp.sendConversion(action: .watched, valueType: .quantity, value: 1, completion: nil)
			}
		}

		// Persist the experiences that aren't alerts since we just processed those.

		let experiences = Set(experiences.filter { $0.experienceType != .alert })
		var array = UserDefaults.standard.array(forKey: keyExperiences) as? [Data] ?? []
		for experience in experiences {
			array.append(NSKeyedArchiver.archivedData(withRootObject: experience))
		}
		UserDefaults.standard.set(array, forKey: keyExperiences)

		// Process the persisted experiences if the app isn't in the background.

		if !inBackground {
			processPersistedExperiences()
		}
	}

	class func go() {
		// Create the shared instance, if needed.
		_ = shared
	}

	@objc
	private func onAppWillEnterForeground() {
		processPersistedExperiences()
	}

	private func processPersistedExperiences() {

		// Load the experiences from user defaults.

		var experiences = [FTMExperience]()
		for data in UserDefaults.standard.array(forKey: keyExperiences) as? [Data] ?? [] {
			if let experience = NSKeyedUnarchiver.unarchiveObject(with: data) as? FTMExperience {
				experiences.append(experience)
			}
		}

		// Process each experience. Handle the types that are appropriate for your app. Send a
		// conversion for each experience when appropriate.

		for exp in experiences {
			if exp.experienceType == .custom {
				// Custom experiences are a common use case. The content property has no
				// predefined meaning.
			}
			else if exp.experienceType == .image {
				// Handle the image. The content property has the URL.
			}
			else {
				// etc...
			}

			let description: String
			switch exp.experienceType {
				case .alert:
					description = "alert"
				case .custom:
					description = "custom"
				case .html:
					description = "html"
				case .image:
					description = "image"
				case .url:
					description = "url"
				case .video:
					description = "video"
			}
			NotificationCenter.default.post(name: ExperienceManager.eventDidProcessExperience,
				object: self, userInfo: ["type": description])
		}

		// Delete the persisted experiences.

		UserDefaults.standard.set([], forKey: keyExperiences)
	}

}

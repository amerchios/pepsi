//
//  BeaconManager.h
//  SampleAppObjC
//
//  Created by Shane Meyer on 5/5/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconManager : NSObject

@property (class, nonatomic, readonly) NSNotificationName eventDidRangeBeacons;

+ (void)go;

@end

//
//  ExperienceManager.m
//  SampleAppObjC
//
//  Created by Shane Meyer on 7/10/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import "ExperienceManager.h"
#import "ViewController.h"

@import FTMCore;

static NSString *keyExperiences = @"SampleAppExperiences";
static ExperienceManager *shared = nil;

@interface ExperienceManager() <FTMExperienceManagerDelegate>

@end

@implementation ExperienceManager

- (instancetype)init {
	if (self = [super init]) {
		FTMExperienceManager.delegate = self;
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self selector:@selector(onAppWillEnterForeground)
			name:UIApplicationWillEnterForegroundNotification object:nil];
	}
	return self;
}

+ (void)initialize {
	shared = [[ExperienceManager alloc] init];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (NSNotificationName)eventDidProcessExperience {
	return @"Sample.ExperienceManagerDidProcessExperience";
}

- (void)experienceManagerDidReceiveExperiences:(NSSet<FTMExperience *> *)experiences {
	UIApplicationState state = [UIApplication sharedApplication].applicationState;
	BOOL inBackground = (state == UIApplicationStateBackground);

	// Process experiences with prompts.

	for (FTMExperience *exp in experiences) {
		if (exp.promptTitle.length == 0 && exp.promptDescription.length == 0) {
			continue;
		}

		if (inBackground) {
			UILocalNotification *n = [[UILocalNotification alloc] init];
			n.alertTitle = exp.promptTitle;
			n.alertBody = exp.promptDescription;
			[[UIApplication sharedApplication] presentLocalNotificationNow:n];
		}
		else {
			UIAlertController *vc = [UIAlertController alertControllerWithTitle:exp.promptTitle
				message:exp.promptDescription preferredStyle:UIAlertControllerStyleAlert];
			[vc addAction:[UIAlertAction actionWithTitle:@"OK"
				style:UIAlertActionStyleDefault handler:nil]];
			[ViewController.instance presentViewController:vc animated:YES completion:nil];
		}

		if (exp.experienceType == FTMExperienceTypeAlert) {

			// Alert experiences have no further information to share. Send a conversion to inform
			// the SmartConnect Cloud. Specify an action and value appropriate for your app.

			[exp sendConversionWithAction:FTMExperienceActionWatched
				valueType:FTMExperienceValueTypeQuantity value:1 completion:nil];
		}
	}

	// Persist the experiences that aren't alerts since we just processed those.

	NSMutableArray<FTMExperience *> *experiencesToKeep = [[NSMutableArray alloc] init];
	for (FTMExperience *exp in experiences) {
		if (exp.experienceType != FTMExperienceTypeAlert) {
			[experiencesToKeep addObject:exp];
		}
	}
	NSArray *array = [[NSUserDefaults standardUserDefaults] arrayForKey:keyExperiences] ?: @[];
	NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:array];
	for (FTMExperience *exp in experiencesToKeep) {
		[mutableArray addObject:[NSKeyedArchiver archivedDataWithRootObject:exp]];
	}
	[[NSUserDefaults standardUserDefaults] setObject:mutableArray forKey:keyExperiences];

	// Process the persisted experiences if the app isn't in the background.

	if (!inBackground) {
		[self processPersistedExperiences];
	}
}

+ (void)go {
	// Calling this causes the initialize method to create the singleton if needed.
}

- (void)onAppWillEnterForeground {
	[self processPersistedExperiences];
}

- (void)processPersistedExperiences {

	// Load the experiences from user defaults.

	NSMutableArray<FTMExperience *> *experiences = [[NSMutableArray alloc] init];
	NSArray *array = [[NSUserDefaults standardUserDefaults] arrayForKey:keyExperiences] ?: @[];
	for (NSData *data in array) {
		FTMExperience *experience = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		if ([experience isKindOfClass:[FTMExperience class]]) {
			[experiences addObject:experience];
		}
	}

	// Process each experience. Handle the types that are appropriate for your app. Send a
	// conversion for each experience when appropriate.

	for (FTMExperience *exp in experiences) {
		if (exp.experienceType == FTMExperienceTypeCustom) {
			// Custom experiences are a common use case. The content property has no
			// predefined meaning.
		}
		else if (exp.experienceType == FTMExperienceTypeImage) {
			// Handle the image. The content property has the URL.
		}
		else {
			// etc...
		}

		NSString *description = @"";
		switch (exp.experienceType) {
			case FTMExperienceTypeAlert:
				description = @"alert";
				break;
			case FTMExperienceTypeCustom:
				description = @"custom";
				break;
			case FTMExperienceTypeHtml:
				description = @"html";
				break;
			case FTMExperienceTypeImage:
				description = @"image";
				break;
			case FTMExperienceTypeUrl:
				description = @"url";
				break;
			case FTMExperienceTypeVideo:
				description = @"video";
				break;
		}

		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc postNotificationName:ExperienceManager.eventDidProcessExperience object:self
			userInfo:@{@"type" : description}];
	}

	// Delete the persisted experiences.

	[[NSUserDefaults standardUserDefaults] setObject:@[] forKey:keyExperiences];
}

@end

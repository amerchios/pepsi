//
//  ExperienceManager.h
//  SampleAppObjC
//
//  Created by Shane Meyer on 7/10/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExperienceManager : NSObject

@property (class, nonatomic, readonly) NSNotificationName eventDidProcessExperience;

+ (void)go;

@end

//
//  AppDelegate.m
//  SampleAppObjC
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "BeaconManager.h"
#import "ExperienceManager.h"
#import "ViewController.h"

@import FTMCore;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
	didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	self.window = [[UIWindow alloc] init];
	self.window.rootViewController = [[ViewController alloc] init];
	[self.window makeKeyAndVisible];

	// Insert your app key and secret below.

	[FTMSession signInWithAppKey:@"" appSecret:@"" username:nil completion:^(NSError *error) {
		if (error != nil) {
			if (ViewController.instance != nil) {
				[AppDelegate showError:error];
			}
			else {
				NSLog(@"Sign-in error: %@", error);
			}
		}
		else {
			// Sign-in was successful. Now other FTMCore classes can be called.
			[BeaconManager go];
			[ExperienceManager go];
		}
	}];

	return YES;
}

+ (void)showError:(NSError *)error {
	[self showMessage:error.localizedDescription];
}

+ (void)showMessage:(NSString *)message {
	UIAlertController *vc = [UIAlertController alertControllerWithTitle:@"" message:message
		preferredStyle:UIAlertControllerStyleAlert];
	[vc addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
	[[ViewController instance] presentViewController:vc animated:YES completion:nil];
}

@end

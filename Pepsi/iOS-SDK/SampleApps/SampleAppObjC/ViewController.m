//
//  ViewController.m
//  SampleAppObjC
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "BeaconManager.h"
#import "ExperienceManager.h"

@import FTMCore;

@interface ViewController () {
	@private __weak UILabel *label;
	@private NSMutableArray<NSString *> *processedExperienceTypes;
}

@end

@implementation ViewController

- (instancetype)init {
	if (self = [super initWithNibName:nil bundle:nil]) {
		processedExperienceTypes = [[NSMutableArray alloc] init];

		// Register for user notifications as part of the demonstration of "alert" experiences.

		UIUserNotificationSettings *settings = [UIUserNotificationSettings
			settingsForTypes:UIUserNotificationTypeAlert categories:nil];
		[[UIApplication sharedApplication] registerUserNotificationSettings:settings];

		// Register for beacon manager ranging and processed experiences.

		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self selector: @selector(onBeaconManagerDidRangeBeacons)
			name: BeaconManager.eventDidRangeBeacons object: nil];
		[nc addObserver:self selector: @selector(onExperienceManagerDidProcessExperience:)
			name: ExperienceManager.eventDidProcessExperience object: nil];
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (ViewController *)instance {
	id appDelegate = [UIApplication sharedApplication].delegate;
	if ([appDelegate isKindOfClass:[AppDelegate class]]) {
		UIWindow *window = ((AppDelegate *)appDelegate).window;
		if (window != nil) {
			id vc = window.rootViewController;
			if ([vc isKindOfClass:[ViewController class]]) {
				return vc;
			}
		}
	}
	return nil;
}

- (void)loadView {
	self.view = [[UIView alloc] init];
	self.view.backgroundColor = UIColor.whiteColor;

	UILabel *lbl = [[UILabel alloc] init];
	label = lbl;
	lbl.font = [UIFont fontWithName:@"Menlo-Regular" size:20] ?: [UIFont systemFontOfSize:20];
	lbl.numberOfLines = 0;
	lbl.textAlignment = NSTextAlignmentLeft;
	lbl.textColor = UIColor.blackColor;
	[self.view addSubview:lbl];

	[self updateUI];
}

- (void)onBeaconManagerDidRangeBeacons {
	[self updateUI];
}

- (void)onExperienceManagerDidProcessExperience:(NSNotification *)notification {
	[processedExperienceTypes addObject:notification.userInfo[@"type"]];
	[self updateUI];
}

- (void)updateUI {
	NSMutableString *ms = [NSMutableString stringWithString:@"Beacons in Range\n----------------"];

	if (FTMSession.isSignedIn) {
		NSArray *beacons = [FTMBeaconManager.beacons.allObjects sortedArrayUsingComparator:
			^NSComparisonResult(FTMBeacon *beacon0, FTMBeacon *beacon1)
		{
			return [beacon0.macAddress compare:beacon1.macAddress];
		}];

		for (FTMBeacon *beacon in beacons) {
			[ms appendString:@"\n"];
			[ms appendString:beacon.macAddress];
		}
	}

	[ms appendString:@"\n\nProcessed Experiences\n---------------------"];
	for (NSString *experienceType in processedExperienceTypes) {
		[ms appendString:@"\n"];
		[ms appendString:experienceType];
	}

	label.text = ms;
	[self.viewIfLoaded setNeedsLayout];
}

- (void)viewDidLayoutSubviews {
	CGSize size = self.view.bounds.size;
	label.frame = CGRectMake(0, 0, 1024, 1);
	[label sizeToFit];
	label.frame = CGRectOffset(label.bounds, round((size.width - label.bounds.size.width) / 2), 64);
}

@end

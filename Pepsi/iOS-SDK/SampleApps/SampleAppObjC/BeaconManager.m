//
//  BeaconManager.m
//  SampleAppObjC
//
//  Created by Shane Meyer on 5/5/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import "BeaconManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>

@import FTMCore;

static BeaconManager *shared = nil;

@interface BeaconManager() <FTMBeaconManagerDelegate>

@end

@implementation BeaconManager

- (instancetype)init {
	if (self = [super init]) {
		FTMBeaconManager.delegate = self;
	}
	return self;
}

+ (void)initialize {
	shared = [[BeaconManager alloc] init];
}

- (void)beaconManagerDidChangeLocationManagerAuthorizationStatus {
	CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
	NSLog(@"The location manager authorization status is now %d.", status);
}

- (void)beaconManagerDidFailWithError:(NSError *)error {
	NSLog(@"Beacon manager error: %@", error);
}

- (void)beaconManagerDidRangeBeacons {
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:[BeaconManager eventDidRangeBeacons] object:nil];
}

- (void)beaconManagerDidUpdateCentralManagerState {
	CBCentralManagerState state = FTMBeaconManager.centralManagerState;
	NSLog(@"The central manager state is now %ld.", (long)state);
}

+ (NSNotificationName)eventDidRangeBeacons {
	return @"Sample.BeaconManagerDidRangeBeacons";
}

+ (void)go {
	// Calling this causes the initialize method to create the singleton if needed.
}

@end

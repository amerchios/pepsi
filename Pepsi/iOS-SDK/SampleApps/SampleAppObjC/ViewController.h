//
//  ViewController.h
//  SampleAppObjC
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (class, nonatomic, readonly) ViewController *instance;

@end

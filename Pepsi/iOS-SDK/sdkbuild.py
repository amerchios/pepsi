import os
import shutil
import subprocess

########################################################################################
#                                                                                      #
#  Builds the Footmarks SDK. Requires python 3.3. The output is a file named           #
#  FootmarksSDK-{version].zip placed on your desktop.                                  #
#                                                                                      #
########################################################################################

def main():

	# Read the framework version from Info.plist.
	infoPath = os.path.abspath("FTMCore/Info.plist")
	version = subprocess.check_output(['defaults', 'read', infoPath, 'CFBundleShortVersionString'])
	version = version.decode().strip()

	# Define some constants.
	destFolderName = 'FootmarksSDK-' + version
	destPath = '~/Desktop/' + destFolderName
	framework = 'FTMCore.framework'
	mode = 'Release'
	project = 'FTMCore.xcodeproj'
	target = 'FTMCore'

	# Clean up before starting.
	os.system('rm -rf ' + destPath)
	os.system('rm -f ' + destPath + '.zip')
	os.system('rm -rf build')

	# Build the framework for device.
	os.system('xcodebuild clean build ONLY_ACTIVE_ARCH=NO \
		-configuration ' + mode + ' \
		-project ' + project + ' \
		-sdk iphoneos \
		-target ' + target)

	# Build the framework for simulator.
	os.system('xcodebuild clean build ONLY_ACTIVE_ARCH=NO \
		-configuration ' + mode + ' \
		-project ' + project + ' \
		-sdk iphonesimulator \
		-target ' + target)

	# Merge the two builds to create a fat binary.
	os.system('lipo -create \
		build/' + mode + '-iphoneos/' + framework + '/' + target + ' \
		build/' + mode + '-iphonesimulator/' + framework + '/' + target + ' \
		-output build/' + target)

	# Set up the directory structure on the desktop and prepare the final framework.
	os.system('mkdir -p ' + destPath + '/dSYM/Device')
	os.system('mkdir    ' + destPath + '/dSYM/Simulator')
	os.system('mv build/' + mode + '-iphoneos/' + framework + ' ' + destPath)
	os.system('mv build/' + mode + '-iphoneos/' + framework + '.dSYM ' + destPath + '/dSYM/Device')
	os.system('mv build/' + mode + '-iphonesimulator/' + framework + '.dSYM ' + destPath + '/dSYM/Simulator')
	os.system('cp -R build/' + mode + '-iphonesimulator/' + framework + '/Modules/' + target + '.swiftmodule ' +
		destPath + '/' + framework + '/Modules')
	os.system('mv build/' + target + ' ' + destPath + '/' + framework)

	# Copy the documentation home page and sample apps.
	os.system('cp -R Documentation.html ' + destPath)
	os.system('cp -R SampleApps ' + destPath)

	# Generate the documentation if jazzy is available.
	if shutil.which('jazzy'):
		os.system('rm -rf Documentation')
		os.system('jazzy')
		os.system('rm -rf Documentation/docsets')
		os.system('rm -f Documentation/undocumented.json')
	os.system('cp -R Documentation ' + destPath)

	# Remove unwanted files and directories.
	os.system('find ' + destPath + ' -name .DS_Store -delete')
	os.system('find ' + destPath + ' -name project.xcworkspace -type d -exec rm -r {} +')
	os.system('find ' + destPath + ' -name xcuserdata -type d -exec rm -r {} +')

	# Zip and clean up.
	os.system('cd ~/Desktop && zip -r9 ' + destFolderName + '.zip ' + destFolderName)
	os.system('rm -rf ' + destPath)
	os.system('rm -rf build')

main()

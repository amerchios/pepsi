//
//  ViewController.swift
//  Pepsi
//
//  Created by Shane Meyer on 7/14/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	private weak var dashboard: Dashboard?
	private weak var imageView: UIImageView?
	private(set) static weak var instance: ViewController?
	private weak var label: UILabel?

	init() {
		super.init(nibName: nil, bundle: nil)
		ViewController.instance = self
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func loadView() {
		view = UIView()
		view.backgroundColor = .white

		let imageView = UIImageView(image: UIImage(named: "Logo"))
		self.imageView = imageView
		view.addSubview(imageView)

		let dashboard = Dashboard()
		self.dashboard = dashboard
		view.addSubview(dashboard)

		let label = UILabel()
		self.label = label
		label.font = UIFont(name: "AvenirNext-Regular", size: 40)
		label.text = AGKLocStr("MAIN_FOOTER")
		label.textColor = Colors.red
		label.sizeToFit()
		view.addSubview(label)
	}

	override func viewDidLayoutSubviews() {
		guard
			let dashboard = dashboard,
			let imageView = imageView,
			let label = label
		else { return }

		let size = view.bounds.size

		label.frame = label.bounds.offsetBy(
			dx: round((size.width - label.bounds.width) / 2),
			dy: size.height - 24 - label.bounds.height)

		dashboard.frame = dashboard.bounds.offsetBy(
			dx: round((size.width - dashboard.bounds.width) / 2),
			dy: label.frame.origin.y - 16 - dashboard.bounds.height)

		let y0 = topLayoutGuide.length
		let y9 = label.frame.origin.y - imageView.bounds.height
		imageView.frame = imageView.bounds.offsetBy(
			dx: (size.width - imageView.bounds.width) / 2,
			dy: y0 + CGFloat(0.2) * (y9 - y0))
	}

}

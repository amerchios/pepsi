//
//  BasicAlert.swift
//  Pepsi
//
//  Created by Shane Meyer on 7/14/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import UIKit

class BasicAlert {

	class func showError(_ error: Error, viewController: UIViewController) {
		showMessage(error.localizedDescription, viewController: viewController)
	}

	class func showMessage(_ message: String, viewController: UIViewController) {
		let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: AGKLocStr("OK"), style: .default))
		viewController.present(alert, animated: true, completion: nil)
	}

}

//
//  Dashboard.swift
//  Pepsi
//
//  Created by Shane Meyer on 7/14/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import CoreLocation
import FTMCore

class Dashboard: UIView, CLLocationManagerDelegate, FTMBeaconManagerDelegate {

	private let alphaDim = CGFloat(0.2)
	private var buttonBeaconCount = UIButton()
	private var buttonBluetooth = UIButton()
	private var buttonLocation = UIButton()
	private var didSignIn = false
	private let errorPlate = UIView()
	private let labelBeaconCount = UILabel()
	private let labelError = UILabel()
	private let labelLocation = UILabel()
	private let locationManager = CLLocationManager()

	init() {
		let d = CGFloat(44)
		let gap = CGFloat(16)
		let width = d + gap + d + gap + d

		super.init(frame: CGRect(x: 0, y: 0, width: width, height: d))

		labelError.font = .boldSystemFont(ofSize: 14)
		labelError.numberOfLines = 0
		labelError.text = AGKLocStr("DASHBOARD_ERROR_PLATE")
		labelError.textColor = .white
		labelError.frame = CGRect(x: 0, y: 0, width: 272, height: 1)
		labelError.sizeToFit()

		errorPlate.backgroundColor = Colors.red
		errorPlate.layer.cornerRadius = 8
		errorPlate.frame = labelError.bounds.insetBy(dx: -12, dy: -8)
		errorPlate.frame = errorPlate.bounds.offsetBy(
			dx: round((width - errorPlate.bounds.width) / 2),
			dy: -errorPlate.bounds.height - 20)

		labelError.frame = labelError.bounds.offsetBy(
			dx: round((errorPlate.bounds.width - labelError.bounds.width) / 2),
			dy: round((errorPlate.bounds.height - labelError.bounds.height) / 2))

		errorPlate.addSubview(labelError)
		addSubview(errorPlate)

		buttonBeaconCount = addButton(diameter: d, icon: nil, offset: nil)
		buttonBeaconCount.alpha = alphaDim
		buttonBeaconCount.isEnabled = true

		buttonBluetooth = addButton(diameter: d, icon: whiteImage(named: "IconBluetooth"),
			offset: .zero)
		buttonLocation = addButton(diameter: d, icon: whiteImage(named: "IconLocation"),
			offset: .zero)

		buttonBluetooth.frame = buttonBluetooth.bounds
		var x = buttonBluetooth.frame.maxX + gap
		buttonLocation.frame = buttonLocation.bounds.offsetBy(dx: x, dy: 0)
		x = buttonLocation.frame.maxX + gap
		buttonBeaconCount.frame = buttonBeaconCount.bounds.offsetBy(dx: x, dy: 0)

		labelBeaconCount.alpha = 0
		labelBeaconCount.font = .boldSystemFont(ofSize: 20)
		labelBeaconCount.frame = buttonBeaconCount.frame
		labelBeaconCount.textAlignment = .center
		labelBeaconCount.textColor = .white
		addSubview(labelBeaconCount)

		labelLocation.font = .boldSystemFont(ofSize: 12)
		labelLocation.text = "1234567890m"
		labelLocation.textAlignment = .center
		labelLocation.textColor = .black
		labelLocation.sizeToFit()
		labelLocation.text = ""
		labelLocation.frame = labelLocation.bounds.offsetBy(
			dx: round(buttonLocation.center.x - labelLocation.bounds.width / 2),
			dy: buttonLocation.frame.maxY + 4)
		addSubview(labelLocation)

		updateUI()

		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.startUpdatingLocation()

		NotificationCenter.default.addObserver(self, selector: #selector(onAppDelegateDidSignIn),
			name: AppDelegate.eventDidSignIn, object: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	private func addButton(diameter: CGFloat, icon: UIImage?, offset: CGPoint?) -> UIButton {
		let button = UIButton(type: .custom)
		let size = CGSize(width: diameter, height: diameter)

		UIGraphicsBeginImageContextWithOptions(size, false, 0)
			Colors.red.setFill()
			UIBezierPath(ovalIn: CGRect(origin: .zero, size: size)).fill()
			if let icon = icon {
				var p = CGPoint(
					x: round((size.width - icon.size.width) / 2),
					y: round((size.height - icon.size.height) / 2))
				if let offset = offset {
					p.x += offset.x
					p.y += offset.y
				}
				icon.draw(at: p)
			}
			var image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
			button.setImage(image, for: .normal)
		UIGraphicsEndImageContext()

		UIGraphicsBeginImageContextWithOptions(size, false, 0)
			image.draw(at: .zero, blendMode: .normal, alpha: alphaDim)
			image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
			button.setImage(image, for: .disabled)
		UIGraphicsEndImageContext()

		button.isEnabled = false
		button.isUserInteractionEnabled = false
		button.sizeToFit()

		addSubview(button)
		return button
	}

	func beaconManagerDidChangeLocationManagerAuthorizationStatus() {
		updateUI()
	}

	func beaconManagerDidFail(error: Error) {
		NSLog("Beacon manager error: " + error.localizedDescription)
	}

	func beaconManagerDidRangeBeacons() {
		let count = FTMBeaconManager.beacons.count
		labelBeaconCount.text = "\(count)"
		if count > 0 {
			buttonBeaconCount.alpha = 1
			labelBeaconCount.alpha = 1
			UIView.animate(withDuration: 3, delay: 0, options: .curveEaseOut, animations: {
				self.buttonBeaconCount.alpha = self.alphaDim
				self.labelBeaconCount.alpha = 0
			})
		}
	}

	func beaconManagerDidUpdateCentralManagerState() {
		updateUI()
	}

	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		assert(Thread.isMainThread)
		guard let location = locations.last else { return }
		labelLocation.text = "\(Int(location.horizontalAccuracy))m"
	}

	@objc
	private func onAppDelegateDidSignIn() {
		FTMBeaconManager.delegate = self
		updateUI()
		weak var wself = self
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			wself?.showBluetoothPromptIfNeeded()
		}
	}

	private func showBluetoothPromptIfNeeded() {
		guard FTMBeaconManager.centralManagerState == .poweredOff else { return }
		var didShow = false

		if let vc = ViewController.instance {
			if vc.presentedViewController is UIAlertController {
				// Only show one alert at a time.
			}
			else {
				BasicAlert.showMessage(AGKLocStr("ERROR_BLUETOOTH_POWERED_OFF"), viewController: vc)
				didShow = true
			}
		}

		if !didShow {
			weak var wself = self
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				wself?.showBluetoothPromptIfNeeded()
			}
		}
	}

	private func updateUI() {
		buttonLocation.isEnabled = (CLLocationManager.authorizationStatus() == .authorizedAlways)
		weak var wself = self

		if FTMSession.isSignedIn {
			if !didSignIn {
				didSignIn = true
				DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
					wself?.updateUI()
				}
			}
			else {
				buttonBluetooth.isEnabled = (FTMBeaconManager.centralManagerState == .poweredOn)
				errorPlate.isHidden = buttonLocation.isEnabled && buttonBluetooth.isEnabled
			}
		}
		else {
			errorPlate.isHidden = true
			DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
				wself?.updateUI()
			}
		}
	}

	private func whiteImage(named imageName: String) -> UIImage? {
		guard var image = UIImage(named: imageName) else { return nil }
		UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
		UIColor.white.setFill()
		UIRectFill(CGRect(origin: .zero, size: image.size))
		image.draw(at: .zero, blendMode: .destinationIn, alpha: 1)
		image = UIGraphicsGetImageFromCurrentImageContext() ?? image
		UIGraphicsEndImageContext()
		return image
	}

}

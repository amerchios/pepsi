//
//  AppDelegate.swift
//  Pepsi
//
//  Created by Shane Meyer on 7/14/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

import FTMCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	static let eventDidSignIn = Notification.Name("PEP.AppDelegateDidSignIn")
	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions
		launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
	{
		let window = UIWindow()
		self.window = window
		window.rootViewController = ViewController()
		window.makeKeyAndVisible()

		signIn()
		stayAlive()

		return true
	}

	private func signIn() {
		weak var wself = self

		FTMSession.signIn(
			appKey: "AHP2641298JDR",
			appSecret: "zE5gNs6WXqB9ih2FBeKFErywJggzo5aj",
			username: "__TurnOffIDFA__")
		{
			(error: Error?) in
			if let error = error {
				if let vc = ViewController.instance {
					if vc.presentedViewController is UIAlertController {
						// Only show one alert at a time.
					}
					else {
						BasicAlert.showError(error, viewController: vc)
					}
				}
				DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
					wself?.signIn()
				}
			}
			else {
				NotificationCenter.default.post(name: AppDelegate.eventDidSignIn, object: nil)
			}
		}
	}

	private func stayAlive() {

		// Start a background task that keeps the app alive for a while. This allows the location
		// manager to acquire a more accurate location, and provides additional time for server
		// pulse and validate calls.

		let taskID = UIApplication.shared.beginBackgroundTask(withName: "StayAlive")

		DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
			NSLog("Ending the stay alive task.")
			UIApplication.shared.endBackgroundTask(taskID)
		}
	}

}

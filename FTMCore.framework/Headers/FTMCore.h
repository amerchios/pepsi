//
//  FTMCore.h
//  FTMCore
//
//  Created by Shane Meyer on 5/2/17.
//  Copyright © 2017 Footmarks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "__FTMPrivate.h"

//! Project version number for FTMCore.
FOUNDATION_EXPORT double FTMCoreVersionNumber;

//! Project version string for FTMCore.
FOUNDATION_EXPORT const unsigned char FTMCoreVersionString[];
